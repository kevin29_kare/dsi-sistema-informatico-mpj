"""
Nombre del Módulo: Archivo de Vistas para la app “control”
Autores: Francisco Rivas, Kevin Rodríguez, Irving Oliva, Fernando Rivas
Última fecha de modificación: 04/10/2017
"""
import datetime
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render,reverse, redirect, get_object_or_404
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from control.models import PlanTratamiento, Medicamento, Examen,Resultado, LoteMedicamento, PlanTratamiento, Receta, Dosis, CITA_CHOICES
from administracion.models import Empleado, Tarifa, Socioeconomico, Expediente
from django import forms
from django.template import RequestContext
from django.http import HttpResponseRedirect
from .forms import *
from itertools import chain
from django.http import HttpResponseRedirect
from django.db.models import Q #Para condiciones OR en Queryset
import locale
# Necesario para la generación de PDF
from django.views import View
from wkhtmltopdf.views import PDFTemplateResponse
from administracion.models import Paciente
# Create your views here.

# Inician Vistas de CitaMédica
class AgregarCita(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    form_class = AgregarCitaForm
    template_name = 'control/agregar_cita.html'
    success_url = reverse_lazy('agregar_cita')
    success_message = "Cita registrada con éxito."
    permission_required = 'control.add_citamedica'

    def get_form(self):
        form = super(AgregarCita, self).get_form()
        #Desplegar Empleados que tienen asignada una especialidad (Es decir, que sean Médicos)
        form.fields['medico'].widget = forms.Select(choices=((x.id, x) for x in Empleado.objects.filter(especialidad_id__isnull=False)))
        # Desplegar Planes de Tratamiento con estado "En tratamiento"
        form.fields['plan_tratamiento'].widget = forms.Select(choices=[('', 'Paciente Externo')] + list((((x.id, x) for x in PlanTratamiento.objects.filter(estado=1)))))
        form.fields['plan_tratamiento'].help_text = "Digite el Número de Expediente y escoja el Plan de Tratamiento deseado."
        return form

@login_required
def agenda_citas(request):
    mensaje = ""
    locale.setlocale(locale.LC_ALL, "es_ES.utf8") # Cambiar el idioma de los meses y días en la agenda de citas
    if request.user.has_perm('control.add_citamedica'):
        if request.POST.get('search'):
            if request.POST.get('select') == '1':
                #Buscar Citas por Médico
                citas = list(CitaMedica.objects.raw("SELECT control_citamedica.id, control_citamedica.fecha, administracion_empleado.nombre FROM control_citamedica LEFT JOIN administracion_empleado ON (control_citamedica.medico_id = administracion_empleado.id) where (control_citamedica.estado = '1' OR control_citamedica.estado = '2') and administracion_empleado.nombre LIKE %s", ['%' + request.POST.get('search') + '%']))
            else:
                #Buscar Citas por Número de Expediente
                citas = list(CitaMedica.objects.raw("SELECT control_citamedica.id, control_citamedica.fecha, administracion_expediente.num_expediente FROM control_citamedica LEFT JOIN control_plantratamiento ON (control_citamedica.plan_tratamiento_id = control_plantratamiento.id) LEFT JOIN administracion_expediente ON (control_plantratamiento.expediente_id = administracion_expediente.id) where (control_citamedica.estado = '1' OR control_citamedica.estado = '2') and administracion_expediente.num_expediente LIKE %s", ['%' + request.POST.get('search') + '%']))
        else:
            citas = CitaMedica.objects.filter(Q(estado = '1') | Q(estado = '2'))
    else:
        try:
            citas=CitaMedica.objects.filter(medico = Empleado.objects.get(user = request.user.id).id).filter(Q(estado = '1') | Q(estado = '2'))
        except:
            citas = []
    agenda = []
    if citas:
        fecha_str = []
        cita_list = []
        count = 0
        fecha = citas[0].fecha
        ultima_cita = citas[list(citas).__len__() - 1]
        for cita in citas:
            if fecha == cita.fecha:
                cita_list.append(cita)
                count += 1
            else:
                fecha_str.append(fecha.strftime("%A"))
                fecha_str.append(fecha.strftime("%d"))
                fecha_str.append(fecha.strftime("%B, %Y"))
                agenda.append([fecha_str, cita_list, count])
                fecha_str = []
                cita_list = []
                count = 0
                fecha = cita.fecha
                cita_list.append(cita)
                count += 1
            if cita == ultima_cita:
                fecha_str.append(fecha.strftime("%A"))
                fecha_str.append(fecha.strftime("%d"))
                fecha_str.append(fecha.strftime("%B, %Y"))
                agenda.append([fecha_str, cita_list, count])
    else:
        # Si el empleado actual no es médico o es médico pero no tiene citas asignadas
        mensaje="No hay citas asignadas"


    return render(request, 'control/agenda_citas.html', {'mensaje': mensaje, 'agenda': agenda},)

class EliminarCitaMedica(PermissionRequiredMixin, DeleteView):
    model=CitaMedica
    permission_required = 'control.add_citamedica'

    def get_success_url(self):
        return reverse("agenda_citas")

class ActualizarCitaMedica(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = CitaMedica
    form_class = ActualizarCitaForm
    template_name_suffix = '_actualizar'
    success_message = "Cita actualizada con éxito"
    permission_required = 'control.add_citamedica'

    def get_context_data(self, **kwargs):
        context = super(ActualizarCitaMedica, self).get_context_data(**kwargs)
        context['cita_choices'] = CITA_CHOICES
        context['medico_list'] = ((x.id, x.nombre) for x in Empleado.objects.filter(especialidad_id__isnull = False, activo = 1))
        context['plan_list'] = ((x.id, x) for x in PlanTratamiento.objects.filter(estado = '1'))
        context['tarifa_consulta'] = False
        cita=self.get_object()
        try:
            socioeconomico = Socioeconomico.objects.get(expediente_id = cita.plan_tratamiento.expediente)
            if socioeconomico.tarifa:
                context['tarifa_consulta'] = Tarifa.objects.get(pk = socioeconomico.tarifa.id).monto
            examen_tarifa = Examen.objects.raw("SELECT control_examen.id, control_examen.nombre, administracion_tarifa.monto FROM control_examen LEFT JOIN administracion_tarifa ON (control_examen.id = administracion_tarifa.examen_id) where administracion_tarifa.tarifa_padre_id IS NULL OR administracion_tarifa.tarifa_padre_id = " + str(socioeconomico.tarifa.id))
            context['examen_tarifa'] = examen_tarifa
            context['socioeconomico'] = True
        except:
            context['examen_tarifa'] = Examen.objects.all()
            context['socioeconomico'] = False
        return context

    def get_success_url(self):
        return reverse("agenda_citas")

class PuntosVitales(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = CitaMedica
    form_class = PuntosVitalesForm
    #fields = ['peso', 'talla', 'imc', 'temp', 'fc', 'fr', 'pa']
    template_name_suffix = '_agregar_puntos_vitales'
    success_message = "Signos Vitales actualizados con éxito"
    permission_required = 'control.add_citamedica'
    #layout = Layout(Fieldset('Agregar Puntos Vitales:'), Row('peso', 'talla', 'imc', 'temp'), Row('fc', 'fr', 'pa'))

    def get_success_url(self):
        return reverse("agenda_citas")

class Diagnostico(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = CitaMedica
    form_class = DiagnosticoForm
    template_name_suffix = '_agregar_diagnostico'
    success_message = "Diagnostico actualizado con éxito"
    permission_required = 'control.change_citamedica'

    def get_success_url(self):
        return reverse("agenda_citas")

class Diagnostico2(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = CitaMedica
    form_class = DiagnosticoForm
    template_name_suffix = '_agregar_diagnostico'
    success_message = "Diagnostico actualizado con éxito"
    permission_required = 'control.change_citamedica'

    def get_success_url(self, **kwargs):
        plan = CitaMedica.objects.get(id=self.kwargs['pk'])
        # expediente = Expediente.objects.get(id=plan.expediente_id)
        # num_expediente = expediente.num_expediente

        if plan.plan_tratamiento_id:
            return reverse_lazy('agregarRecetaDiagnostico2', kwargs={'pt_id': plan.plan_tratamiento_id})
        else:
            return reverse_lazy('agenda_citas')


class ConsultarDiagnostico(PermissionRequiredMixin, UpdateView):
    model = CitaMedica
    form_class = ConsultarDiagnosticoForm
    template_name_suffix = '_consultar_diagnostico'
    permission_required = 'control.delete_citamedica'

# Finalizan Vistas de CitaMedica

# Inician vistas de Medicamento
class AgregarMedicamento(SuccessMessageMixin, PermissionRequiredMixin,CreateView):
    model = Medicamento
    fields = ['nombre']
    template_name = 'control/agregar_medicamento.html'
    success_message = "Medicamento Agregado con Éxito"
    permission_required = 'control.add_medicamento'
    success_url = reverse_lazy('agregar_medicamento')

class ModificarMedicamento(SuccessMessageMixin, PermissionRequiredMixin,UpdateView):
    model = Medicamento
    fields = ['nombre']
    template_name = 'control/agregar_medicamento.html'
    success_message = "Medicamento Modificado con Éxito"
    permission_required = 'control.change_medicamento'

    def get_context_data(self, **kwargs):
        context = super(ModificarMedicamento, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("consultarMedicamento")


@login_required
def consultarMedicamento(request):
    if request.user.has_perm('control.change_medicamento') or request.user.has_perm('control.delete_medicamento'):
        medicamentos_list = Medicamento.objects.all()
        return render(request, 'control/consultar_medicamento.html', {'medicamentos_list': medicamentos_list,})
    else:
        return HttpResponseRedirect('/base/')

@login_required
@permission_required('control.delete_medicamento')
def desactivar_medicamento(request, medicamento_id):
    medicamento = Medicamento.objects.get(pk=medicamento_id)

    if medicamento.estado == '1':
        medicamento.estado = '0'
    else:
        medicamento.estado = '1'
    medicamento.save()
    return HttpResponseRedirect('/medicamentos')

@permission_required('control.delete_medicamento')
@login_required
def confirmar_desactivar_medicamento(request, medicamento_id):
    medicamento=Medicamento.objects.get(pk=medicamento_id)
    mensaje=""
    if medicamento.estado == '1':
        mensaje="desactivar"
    else:
        mensaje="activar"

    return render(request, 'control/modificar_medicamento.html', {'medicamento': medicamento, 'mensaje': mensaje, })
# Finalizan vistas de Medicamento

# Inician vistas de Exámenes Médicos
class AgregarExamen(SuccessMessageMixin, PermissionRequiredMixin,CreateView):
    model = Examen
    fields = ['nombre']
    template_name = 'control/agregar_examen.html'
    success_message = "Examen Agregado con Éxito"
    permission_required = 'control.add_examen'
    success_url = reverse_lazy('agregar_examen')

@login_required
def consultarExamen(request):
    if request.user.has_perm('control.change_examen') or request.user.has_perm('control.delete_examen'):
        examenes_list = Examen.objects.all()
        return render(request, 'control/consultar_examen.html', {'examenes_list': examenes_list,})
    else:
        return HttpResponseRedirect('/base/')


class ModificarExamen(SuccessMessageMixin, PermissionRequiredMixin,UpdateView):
    model = Examen
    fields = ['nombre']
    template_name = 'control/agregar_examen.html'
    success_message = "Examen Modificado con Éxito"
    permission_required = 'control.change_examen'

    def get_context_data(self, **kwargs):
        context = super(ModificarExamen, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("consultarExamen")


@login_required
@permission_required('control.delete_examen')
def desactivar_examen(request, examen_id):
    examen = Examen.objects.get(pk=examen_id)

    if examen.estado == '1':
        examen.estado = '0'
    else:
        examen.estado = '1'
    examen.save()
    return HttpResponseRedirect('/examenes')

@permission_required('control.delete_examen')
@login_required
def confirmar_desactivar_examen(request, examen_id):
    examen=Examen.objects.get(pk=examen_id)
    mensaje=""
    if examen.estado == '1':
        mensaje="desactivar"
    else:
        mensaje="activar"

    return render(request, 'control/modificar_examen.html', {'examen': examen, 'mensaje': mensaje, })
# Finalizan vistas de Exámenes Médicos

# Inician Vistas de Lote de Medicamento
@login_required
@permission_required('control.add_lotemedicamento')
def Agregar_lotes(request,medicamento_id):
    mensaje=""
    if request.method=='GET':
        lote_form=LoteForm(prefix='lote')


    #Cuando es POST
    if request.method=='POST':
        lote_form=LoteForm(request.POST or None, prefix='lote')
        if lote_form.is_valid():
            medi=Medicamento.objects.get(pk=medicamento_id)

            tvencimiento=lote_form.cleaned_data['fecha_vencimiento']
            texitencia= int (lote_form.cleaned_data['existencias'])

            lote=LoteMedicamento.objects.create(
                fecha_vencimiento=tvencimiento,
                existencias=texitencia,
                medicamento=medi,
                cantidad_ingreso=texitencia,
                estado=1
            )
            #Limpiando campos despues de guardar
            medi.stock = medi.stock + texitencia
            medi.save()
            mensaje="Lote Creado con exito"
            lote_form=LoteForm(prefix='lote')


    extra_context = {
        'lote_form':lote_form,
        'mensaje': mensaje,
        'medicamento_id':medicamento_id
    }

    return render(request, 'control/agregar_lotes.html', extra_context)




@login_required
def consultarLotes(request,medicamento_id):
    if request.user.has_perm('control.change_lotemedicamento') or request.user.has_perm('control.delete_'):
        lotes_list = LoteMedicamento.objects.filter(medicamento_id=medicamento_id ).filter(estado=1)
        return render(request, 'control/consultaLotes.html', {'lotes_list': lotes_list, })
    else:
        return HttpResponseRedirect('/login/?next=/lotes/')


@login_required
@permission_required('control.change_lotemedicamento')
def LoteUpdate(request,lote_id ):
    mensaje = ""

    lote=LoteMedicamento.objects.get(pk=lote_id)
    medi2=lote.medicamento
    fecha=lote.fecha_vencimiento
    lote_inicial=lote.existencias

    if request.POST:
        if request.POST.get('fecha_vencimiento') and request.POST.get('existencias'):

            lote_entrante=int(request.POST.get('existencias'))
            lote.existencias=lote_entrante

            #Aqui capturo la fecha
            fecha=request.POST.get('fecha_vencimiento')
            #El separador de la fecha
            slist = fecha.split("/")

            #Aqui convierto la fecha :V
            lote.fecha_vencimiento = datetime.date(int(slist[2]), int(slist[1]), int(slist[0]))






            medi = Medicamento.objects.get(pk=lote.medicamento.id)
            diferencia = lote_entrante-lote_inicial

            if (lote_entrante==0):
                lote.estado=0
            if (diferencia>0):
                lote.cantidad_ingreso=lote.cantidad_ingreso+diferencia


            try:

                medi.stock=medi.stock+diferencia
                lote.save()
                medi.save()
                mensaje = "Lote Modificado con exito"
                fecha=LoteMedicamento.objects.get(pk=lote_id).fecha_vencimiento
                medi2 =LoteMedicamento.objects.get(pk=lote_id).medicamento
            except:
                mensaje = "Error en formato de Datos"

        else:
            mensaje = "Error al Modificar  lote"

    return render(request, 'control/actualizar_lotes.html', {'mensaje': mensaje, 'lote_id': lote_id, 'existencias': lote.existencias, 'fecha_vencimiento':fecha , 'medicamento':medi2.id, })
# Finalizan vistas de Lotes de Medicamento

# Inician vistas de Resultados de Exámenes
@login_required
@permission_required('control.add_resultado')
def AgregarResultado(request,exp_id):
    mensaje=""

    if request.method=='GET':
        resultado_form=ResultadoForm(prefix='resultado')

    #Cuando es POST
    if request.method=='POST':
        resultado_form=ResultadoForm(request.POST or None, prefix='resultado')
        if resultado_form.is_valid():
            exp=Expediente.objects.get(pk=exp_id)

            tlectura=resultado_form.cleaned_data['lectura']
            tfecha_realizacion=resultado_form.cleaned_data['fecha_realizacion']
            texamen=resultado_form.cleaned_data['examen']
            tcomentario=resultado_form.cleaned_data['comentario']

            resultado=Resultado.objects.create(
                lectura=tlectura,
                fecha_realizacion=tfecha_realizacion,
                examen=texamen,
                expediente=exp,
                comentario=tcomentario
            )

            #Limpiando campos despues de guardar
            mensaje="Resultado de Examen creado con éxito"
            resultado_form=ResultadoForm(prefix='resultado')

    extra_context = {
        'resultado_form': resultado_form,
        'mensaje': mensaje,
        'pk': exp_id
    }

    return render(request, 'control/agregar_resultado.html' ,extra_context)

class DetalleResultado(SuccessMessageMixin, PermissionRequiredMixin,DeleteView):
    model=Resultado
    template_name = 'control/detalle_resultado.html'
    permission_required = 'control.change_resultado'


@login_required
def consultarResultados(request,exp_id = None):
    if request.user.has_perm('control.change_resultado') or request.user.has_perm('control.delete_resultado'):
        resultados_lis = Resultado.objects.filter(expediente=exp_id)
        return render(request, 'control/consultaExamenes.html', {'resultados_lis': resultados_lis, })
    else:
        return HttpResponseRedirect('/login/?next=/consultaExamenes/')
# Finalizan vistas de Resultados de Exámenes

# Inician vistas de Planes de Tratamiento
@login_required
@permission_required('control.change_plantratamiento')
def detallePlanes(request, pk = None):
    exp = PlanTratamiento.objects.get(pk = pk).expediente.id
    recetas = Receta.objects.filter(plan_tratamiento_id=pk)
    context = {'recetas': recetas, 'pt_id': pk, 'exp': exp}
    return render(request, 'control/detalle_plan.html', context)

@login_required
@permission_required('control.change_dosis')
def detalleDosis(request, pk = None):
    receta = Receta.objects.get(pk = pk)
    dosis = Dosis.objects.filter(receta_id=pk)
    context = {'dosis': dosis, 'r_id':pk, 'plan': receta.plan_tratamiento_id}
    return render(request, 'control/detalle_dosis.html', context)

class PlanTratamientoCreate(PermissionRequiredMixin, CreateView):
    model = PlanTratamiento
    template_name = "control/agregar_plan.html"
    fields = ['fecha_inicio', 'fecha_fin', 'estado', 'descripcion']
    permission_required = 'control.add_plantratamiento'

    def form_valid(self, form):
        form.instance.expediente = Expediente.objects.get(id=self.kwargs['expediente_id'])
        return super(PlanTratamientoCreate, self).form_valid(form)


class RecetaCreateView(PermissionRequiredMixin, CreateView ):
    model = Receta
    template_name = 'control/agregar_receta.html'
    fields = ['indicaciones']
    permission_required = 'control.add_receta'

    def form_valid(self, form):
        form.instance.plan_tratamiento = PlanTratamiento.objects.get(id=self.kwargs['pt_id'])
        return super(RecetaCreateView, self).form_valid(form)

class DosisCreateView(PermissionRequiredMixin, CreateView):
    model = Dosis
    template_name = 'control/agregar_dosis.html'
    fields = ['cantidad', 'dosis','medicamento']
    permission_required = 'control.add_dosis'

    def form_valid(self, form):
        form.instance.receta = Receta.objects.get(id=self.kwargs['r_id'])
        return super(DosisCreateView, self).form_valid(form)

class PlanTratamientoUpdateView(PermissionRequiredMixin, UpdateView):
    model = PlanTratamiento
    fields = ['fecha_fin', 'estado', 'descripcion']
    template_name = 'control/modificar_plan.html'
    permission_required = 'control.change_plantratamiento'

    def get_object(self, queryset=None):
        plan = PlanTratamiento.objects.get(id=self.kwargs['pk'])
        self.kwargs['exp'] = plan.expediente.id
        return plan

    def get_success_url(self, **kwargs):
        plan = PlanTratamiento.objects.get(id=self.kwargs['pk'])
        # expediente = Expediente.objects.get(id=plan.expediente_id)
        # num_expediente = expediente.num_expediente
        return reverse_lazy('expediente', kwargs={'pk': plan.expediente_id})


class ImprimirRecetaPDFView(View):
    template_name = 'control/imprimir_receta.html'
    def get(self, request, *args, **kwargs):
        receta = Receta.objects.get(pk = kwargs['pk'])
        dosis = Dosis.objects.filter(receta = receta)
        context = {
            'receta' : receta,
            'dosis': dosis,
            'doctor': request.user.empleado,
            'fecha': datetime.date.today
        }
        response = PDFTemplateResponse(request=request, template=self.template_name, filename="receta.pdf",
                                       context=context,
                                       show_content_in_browser=True, cmd_options={
                'margin-top': 10,
                'zoom': 1,
                'viewport-size': '1366 x 513',
                'javascript-delay': 1000,
                'footer-center': '[page]/[topage]',
                'no-stop-slow-scripts': True
            },
                                       )
        return response
# Finalizan vistas de Planes de Tratamiento

# Crear receta desde diagnostico
@login_required
def AgregarRecetaDiagnostico(request,pt_id):
    mensaje=""

    if request.method=='GET':
        receta_form=RecetaForm()

    #Cuando es POST
    if request.method=='POST':
        receta_form=RecetaForm(request.POST or None)
        if receta_form.is_valid():
            plan=PlanTratamiento.objects.get(pk=pt_id)

            tIndicacion=receta_form.cleaned_data['indicaciones']
            receta=Receta.objects.create(
                indicaciones=tIndicacion,
                plan_tratamiento_id=plan.id
            )
            #Limpiando campos despues de guardar
            mensaje="Receta agregada con éxito"
            receta_form=RecetaForm()
            return redirect('agregarDosis', r_id=receta.id)
    extra_context = {
        'diag': True,
        'mensaje': mensaje,
        'form':receta_form,
    }

    return render(request, 'control/agregar_receta.html' ,extra_context)