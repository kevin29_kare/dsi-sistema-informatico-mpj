# -*- coding: utf8 -*-
"""
Nombre del Módulo: Archivo de Modelos para la app “control”
Autores: Francisco Rivas, Kevin Rodríguez
Última fecha de modificación: 04/10/2017
"""

from django.db import models
from administracion.models import Expediente, Empleado
from django.core.validators import MinValueValidator
from django.shortcuts import reverse
from datetime import datetime
from django.utils.dateformat import DateFormat

# Create your models here.
CHOICES = (
    ('1', 'Disponible'),
    ('2', 'No Disponible'),
)
ESTADO_CHOICES = (
    ('1', 'En Tratamiento General'),
    ('2', 'En Tratamiento de Epilepsia'),
    ('2', 'Epilepsia Controlada'),
    ('4', 'Finalizado')
)
CITA_CHOICES = (
    ('1', 'Asignada'),
    ('2', 'En Consulta'),
    ('3', 'Finalizada'),
    ('4', 'Expirada'),
)
class PlanTratamiento(models.Model):
    fecha_inicio = models.DateField('Fecha de inicio', help_text = 'Formato: DD/MM/AAAA', blank = False, null = False)
    fecha_fin = models.DateField('Fecha de finalización', help_text = 'Formato: DD/MM/AAAA', blank = True, null = True)
    estado = models.CharField('estado', max_length = 1, choices = ESTADO_CHOICES, blank = False, null = False, default = ('1'))
    descripcion =  models.CharField('Descripción del Plan de Tratamiento', max_length = 1024, blank = True, null = True)

    expediente = models.ForeignKey(Expediente, blank = False, null = False)

    def __str__(self):
        # return self.expediente.num_expediente + " | Fecha de inicio del tratamiento: " + str(self.fecha_inicio)
        dformat = DateFormat(self.fecha_inicio)
        new_date = dformat.format('d/m/Y')
        return '{} - {} - {}: {}'.format(self.expediente.num_expediente, self.expediente.paciente, 'Inicio del tratamiento', str(new_date))

    def get_absolute_url(self):
        return reverse('detallePlan', args=[str(self.id)])

    def get_estado_display(self):
        for estado in ESTADO_CHOICES:
            if self.estado == estado[0]:
                return estado[1]

    class Meta:
        ordering = ["estado", "fecha_inicio"]
        verbose_name = 'Plan de Tratamiento'
        verbose_name_plural = 'Planes de Tratamiento'

class Receta(models.Model):
    fecha_prescripcion = models.DateTimeField('Fecha', blank = False, null = False, auto_now_add = True)
    indicaciones = models.CharField('Indicaciones', max_length = 1024, blank = False, null = False)
    plan_tratamiento = models.ForeignKey(PlanTratamiento, blank = False, null = False)

    def __str__(self):
        return '{} {}: {}'.format('Expediente', self.plan_tratamiento.expediente.num_expediente, self.indicaciones)

    def get_absolute_url(self):
        return reverse('detallePlan', args=[str(self.plan_tratamiento_id)])

    class Meta:
        ordering = ["fecha_prescripcion"]
        verbose_name = 'Receta'
        verbose_name_plural = 'Recetas'

class Medicamento(models.Model):
    nombre = models.CharField('Nombre del Medicamento', max_length = 64, blank = False, null = False, unique = True)
    stock = models.IntegerField('Existencias Totales', blank = False, null = False, default = 0, validators = [MinValueValidator(0)])
    estado = models.CharField('Estado', max_length = 1, choices = CHOICES, blank = False, null = False,default = ('1'))

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ["estado", "nombre"]
        verbose_name = 'Medicamento'
        verbose_name_plural = 'Medicamentos'


class Dosis(models.Model):
    cantidad=models.IntegerField('Cantidad', blank = False, null = False, validators = [MinValueValidator(0)])
    dosis=models.CharField('Dosis', max_length = 128, blank = False, null = False)
    medicamento = models.ForeignKey(Medicamento, blank = False, null = False)
    receta = models.ForeignKey(Receta, blank = False, null = False)

    def __str__(self):
        return "Medicamento: " + self.medicamento.nombre + ". Dosis: " + self.dosis

    class Meta:
        verbose_name = 'Dosis'
        verbose_name_plural = 'Dosis'

    def get_absolute_url(self):
        return reverse('detalleDosis', args=[str(self.receta_id)])



class LoteMedicamento(models.Model):
    fecha_ingreso = models.DateTimeField('Fecha de Ingreso', blank = False, null = False, auto_now_add = True)
    fecha_vencimiento = models.DateField('Fecha de Vencimiento', help_text = 'Formato: DD/MM/AAAA',blank = False, null = False)
    existencias = models.IntegerField('Existencias del Lote', blank = False, null = False, validators = [MinValueValidator(0)])
    medicamento = models.ForeignKey(Medicamento, blank = False, null = False)
    estado = models.CharField('estado', max_length=1, choices=CHOICES, blank=False, null=False, default=('1'))
    cantidad_ingreso = models.IntegerField('Cantidad que ingresó', blank = False, null = False, validators = [MinValueValidator(0)])

    def __str__(self):
        return "Lote de " + self.medicamento.nombre +". Vence: " + str(self.fecha_vencimiento)
    class Meta:
        ordering = ["medicamento", "fecha_ingreso"]
        verbose_name = 'Lote de Medicamento'
        verbose_name_plural = 'Lotes de Medicamentos'
    @classmethod
    def Lote_create(cls, fecha, exis, id):
        lote = cls(fecha_vencimiento=fecha, existencias=exis, medicamento=id)
        return lote

class Examen(models.Model):
    nombre = models.CharField('Nombre del Examen', max_length = 64, blank = False, null = False, unique = True)
    estado = models.CharField('estado', max_length = 1, choices = CHOICES, blank = False, null = False, default = ('1'))

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ["estado", "nombre"]
        verbose_name = 'Examen'
        verbose_name_plural = 'Exámenes'

class Resultado(models.Model): #Resultado de Examen de Laboratorio (Sólo para pacientes de la Fundación
    lectura = models.CharField('Lectura del Resultado', max_length = 1024, blank = False, null = False)
    fecha_realizacion = models.DateField('Fecha de Realización', help_text = 'Formato: DD/MM/AAAA', blank = False, null = False)
    fecha_lectura = models.DateField('Fecha de Lectura', help_text = 'Formato: DD/MM/AAAA', blank = False, null = False, auto_now_add = True)
    comentario = models.CharField('Comentario del Resultado', max_length = 1024, blank = True, null = True)
    expediente = models.ForeignKey(Expediente, blank = False, null = False)
    examen = models.ForeignKey(Examen, blank = False, null = False)

    def __str__(self):
        return "Lectura de " + self.examen.nombre + " en expediente: " + self.expediente.num_expediente


class CitaMedica(models.Model):
    fecha = models.DateField('Fecha', blank = False, null = False, help_text = 'Formato: DD/MM/AAAA')
    hora_inicio = models.TimeField('Hora de Inicio', blank = False, null = False)
    hora_fin = models.TimeField('Hora de Finalización', blank = False, null = False)
    peso = models.DecimalField('Peso', max_digits = 5, decimal_places = 2, blank = True, null = True, help_text = 'Peso (kg)', validators = [MinValueValidator(0)])
    talla = models.DecimalField('Talla', max_digits = 5, decimal_places = 2, blank = True, null = True, help_text = 'Talla (m)', validators = [MinValueValidator(0)])
    imc = models.DecimalField('IMC', max_digits = 5, decimal_places = 1, blank = True, null = True, help_text = 'Índice de Masa Corporal (kg/m2)', validators = [MinValueValidator(0)])
    temp = models.DecimalField('C°', max_digits = 5, decimal_places = 2, blank = True, null = True, help_text = 'Temperatura (°C)')
    fc = models.IntegerField('FC', blank = True, null = True, help_text = 'Frecuencia Cardíaca (lat/min)', validators = [MinValueValidator(0)])
    fr = models.IntegerField('FR', blank = True, null = True, help_text = 'Frecuencia Respiratoria (resp/min)', validators = [MinValueValidator(0)])
    pa = models.CharField('PA', max_length = 6, blank = True, null = True, help_text = 'Presión Arterial ###/##')
    cx = models.CharField('Cx', max_length = 63, blank = True, null = True, help_text = 'Consulta por:')
    pe = models.TextField('P.E.', max_length = 1024, blank = True, null = True, help_text = 'Presenta Enfermedad')
    ef = models.TextField('Examen Físico', max_length = 1024, blank = True, null = True)
    antecedente = models.TextField('Antecedente', max_length = 1024, blank = True, null = True)
    dx = models.CharField('Dx', max_length = 63, blank = True, null = True, help_text = 'Diagnóstico')
    plan = models.CharField('Plan', max_length = 1024, blank = True, null = True)
    estado = models.CharField('Estado de la Cita', max_length = 1, choices = CITA_CHOICES, blank = False, null = False, default = ('1'))
    precio_consulta = models.DecimalField('Precio de la Consulta', max_digits = 8, decimal_places = 2, blank = True, null = True, validators = [MinValueValidator(0)])
    precio_examen = models.DecimalField('Precio del Examen', max_digits = 8, decimal_places = 2, blank = True, null = True, validators = [MinValueValidator(0)])
    fecha_proxima = models.DateField('Fecha de Próxima Cita', blank=False, null=True, help_text='Formato: DD/MM/AAAA')
    medico = models.ForeignKey(Empleado, blank = False, null = False)
    plan_tratamiento = models.ForeignKey(PlanTratamiento, blank = True, null = True)
    examen = models.ForeignKey(Examen, blank = True, null = True)
    convenio = models.BooleanField('¿Convenio?', default=False)

    def __str__(self):
        return "Cita Médica por médico: " + self.medico.nombre + ", fecha: " + str(self.fecha)

    def get_edad(self):
        diff = (self.fecha - self.plan_tratamiento.expediente.paciente.fecha_nacimiento).days
        edad = str(int(diff / 365)) # Edad en años
        string = ' años'
        if edad == '0':
            edad = str(int(diff / 12))# Edad en meses
            string = ' meses'
        return edad + string

    class Meta:
        ordering=["fecha", "hora_inicio"]
        verbose_name = 'Cita Médica'
        verbose_name_plural = 'Citas Médicas'
