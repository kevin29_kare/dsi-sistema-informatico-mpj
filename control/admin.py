from django.contrib import admin
from .models import PlanTratamiento, Receta, Medicamento, Dosis, LoteMedicamento, Examen, Resultado, CitaMedica

# Register your models here.
admin.site.register(PlanTratamiento)
admin.site.register(Receta)
admin.site.register(Medicamento)
admin.site.register(Dosis)
admin.site.register(LoteMedicamento)
admin.site.register(Examen)
admin.site.register(Resultado)
admin.site.register(CitaMedica)