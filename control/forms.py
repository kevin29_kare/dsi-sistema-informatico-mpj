"""
Nombre del Módulo: Archivo de formularios para la app “control”
Autores: Kevin Rodríguez, Irving Oliva, Fernando Rivas
Última fecha de modificación: 04/10/2017
"""

from django import forms
from material import *
from .models import CitaMedica,Resultado,Examen,LoteMedicamento, Receta, Dosis
import datetime
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q
from administracion.models import Paciente

#Inician Formularios de CitaMedica
class ActualizarCitaForm(forms.ModelForm):
    class Meta:
        model = CitaMedica
        fields = ['fecha', 'hora_inicio', 'hora_fin', 'medico', 'plan_tratamiento', 'estado', 'precio_consulta',
                  'examen', 'precio_examen', 'convenio']

    def clean(self):
        cleaned_data = super(ActualizarCitaForm, self).clean()
        hora_inicio = cleaned_data.get('hora_inicio')
        hora_fin = cleaned_data.get('hora_fin')
        if hora_inicio is not None and hora_fin is not None:
            if hora_inicio >= hora_fin:
                raise ValidationError(
                    {'hora_fin': _('La Hora de Finalización debe ser mayor que la Hora de Inicio')}
                )

        citas = CitaMedica.objects.filter(Q(estado = '1') | Q(estado = '2')).filter(medico = cleaned_data.get('medico')).filter(fecha = cleaned_data.get('fecha')).exclude(pk = self.instance.id)
        print(citas)
        for cita in citas:
            if hora_inicio < cita.hora_inicio:
                if hora_fin > cita.hora_inicio:
                    raise ValidationError(
                        {'hora_fin': _('Ya hay citas asignadas en este horario para este Médico'),
                         'hora_inicio': _('Ya hay citas asignadas en este horario para este Médico')}
                    )
            if hora_inicio == cita.hora_inicio:
                raise ValidationError(
                    {'hora_fin': _('Ya hay citas asignadas en este horario para este Médico'),
                     'hora_inicio': _('Ya hay citas asignadas en este horario para este Médico')}
                )
            if hora_inicio > cita.hora_inicio:
                if hora_inicio < cita.hora_fin:
                    raise ValidationError(
                        {'hora_fin': _('Ya hay citas asignadas en este horario para este Médico'),
                         'hora_inicio': _('Ya hay citas asignadas en este horario para este Médico')}
                    )


class AgregarCitaForm(forms.ModelForm):
    class Meta:
        model = CitaMedica
        fields = ['fecha', 'hora_inicio', 'hora_fin', 'medico', 'plan_tratamiento']

    def clean(self):
        cleaned_data = super(AgregarCitaForm, self).clean()
        hora_inicio = cleaned_data.get('hora_inicio')
        hora_fin = cleaned_data.get('hora_fin')
        if hora_inicio is not None and hora_fin is not None:
            if hora_inicio >= hora_fin:
                raise ValidationError(
                    {'hora_fin': _('La Hora de Finalización debe ser mayor que la Hora de Inicio')}
                )

        citas = CitaMedica.objects.filter(Q(estado = '1') | Q(estado = '2')).filter(medico = cleaned_data.get('medico')).filter(fecha = cleaned_data.get('fecha'))
        for cita in citas:
            if hora_inicio < cita.hora_inicio:
                if hora_fin > cita.hora_inicio:
                    raise ValidationError(
                        {'hora_fin': _('Ya hay citas asignadas en este horario para este Médico'),
                         'hora_inicio': _('Ya hay citas asignadas en este horario para este Médico')}
                    )
            if hora_inicio == cita.hora_inicio:
                raise ValidationError(
                    {'hora_fin': _('Ya hay citas asignadas en este horario para este Médico'),
                     'hora_inicio': _('Ya hay citas asignadas en este horario para este Médico')}
                )
            if hora_inicio > cita.hora_inicio:
                if hora_inicio < cita.hora_fin:
                    raise ValidationError(
                        {'hora_fin': _('Ya hay citas asignadas en este horario para este Médico'),
                         'hora_inicio': _('Ya hay citas asignadas en este horario para este Médico')}
                    )

    layout = Layout(Fieldset('Agregar Cita Médica:'), Row('fecha', 'hora_inicio', 'hora_fin'),
                    Row('medico', 'plan_tratamiento'))


class PuntosVitalesForm(forms.ModelForm):
    fecha = forms.DateField(label = 'Fecha', initial=datetime.date.today, required = False)
    nombre = forms.CharField(label='Nombre del Paciente', required = False)
    num_expediente= forms.CharField(label='Expediente del Paciente', required=False)
    # Esta edad será calculada con la fecha de la cita, para que se mantenga tal cual era para ese entonces
    edad = forms.CharField(label='Edad del Paciente', required=False)
    class Meta:
        model = CitaMedica
        fields = ['peso', 'talla', 'imc']
    layout = Layout(Fieldset('Información Básica:'), Row('fecha', 'num_expediente'), Row('nombre', 'edad'),
                    Fieldset('Agregar Signos Vitales:'), Row('peso', 'talla', 'imc'))


    def __init__(self, *args, **kwargs):
        super(PuntosVitalesForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs['readonly'] = True
        self.fields['fecha'].widget.attrs['readonly'] = True
        self.fields['edad'].widget.attrs['readonly'] = True
        self.fields['num_expediente'].widget.attrs['readonly'] = True


class DiagnosticoForm(forms.ModelForm):
    fecha = forms.DateField(label='Fecha', initial=datetime.date.today, required=False)
    nombre = forms.CharField(label='Nombre del Paciente', required=False)
    num_expediente = forms.CharField(label='Expediente del Paciente', required=False)
    # Esta edad será calculada con la fecha de la cita, para que se mantenga tal cual era para ese entonces
    edad = forms.CharField(label='Edad del Paciente', required=False)

    class Meta:
        model = CitaMedica
        fields = ['peso', 'talla', 'imc', 'temp', 'fc', 'fr', 'pa', 'cx', 'pe', 'ef', 'antecedente', 'dx', 'plan','fecha_proxima']

    layout = Layout(Fieldset('Información Básica:'), Row('fecha', 'num_expediente'), Row('nombre', 'edad'),
                    Fieldset('Puntos Vitales:'), Row('peso', 'talla', 'imc'), Row('temp', 'fc', 'fr', 'pa'),
                    Fieldset('Agregar Diagnóstico: '), 'cx', 'pe', 'ef', 'antecedente', 'dx', 'plan','fecha_proxima')

    def __init__(self, *args, **kwargs):
        super(DiagnosticoForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs['readonly'] = True
        self.fields['fecha'].widget.attrs['readonly'] = True
        self.fields['edad'].widget.attrs['readonly'] = True
        self.fields['num_expediente'].widget.attrs['readonly'] = True
        self.fields['peso'].widget.attrs['readonly'] = True
        self.fields['talla'].widget.attrs['readonly'] = True
        self.fields['imc'].widget.attrs['readonly'] = True
        # self.fields['temp'].widget.attrs['readonly'] = True
        # self.fields['fc'].widget.attrs['readonly'] = True
        # self.fields['fr'].widget.attrs['readonly'] = True
        # self.fields['pa'].widget.attrs['readonly'] = True

class ConsultarDiagnosticoForm(forms.ModelForm):
    fecha = forms.DateField(label='Fecha', initial=datetime.date.today, required=False)
    nombre = forms.CharField(label='Nombre del Paciente', required=False)
    num_expediente = forms.CharField(label='Expediente del Paciente', required=False)
    # Esta edad será calculada con la fecha de la cita, para que se mantenga tal cual era para ese entonces
    edad = forms.CharField(label='Edad del Paciente', required=False)

    class Meta:
        model = CitaMedica
        fields = ['peso', 'talla', 'imc', 'temp', 'fc', 'fr', 'pa', 'cx', 'pe', 'ef', 'antecedente', 'dx', 'plan']

    layout = Layout(Fieldset('Información Básica:'), Row('fecha', 'num_expediente'), Row('nombre', 'edad'),
                    Fieldset('Puntos Vitales:'), Row('peso', 'talla', 'imc'), Row('temp', 'fc', 'fr', 'pa'),
                    Fieldset('Diagnóstico: '), 'cx', 'pe', 'ef', 'antecedente', 'dx', 'plan')

    def __init__(self, *args, **kwargs):
        super(ConsultarDiagnosticoForm, self).__init__(*args, **kwargs)
        self.fields['nombre'].widget.attrs['readonly'] = True
        self.fields['fecha'].widget.attrs['readonly'] = True
        self.fields['edad'].widget.attrs['readonly'] = True
        self.fields['num_expediente'].widget.attrs['readonly'] = True
        self.fields['peso'].widget.attrs['readonly'] = True
        self.fields['talla'].widget.attrs['readonly'] = True
        self.fields['imc'].widget.attrs['readonly'] = True
        self.fields['temp'].widget.attrs['readonly'] = True
        self.fields['fc'].widget.attrs['readonly'] = True
        self.fields['fr'].widget.attrs['readonly'] = True
        self.fields['pa'].widget.attrs['readonly'] = True
        self.fields['cx'].widget.attrs['readonly'] = True
        self.fields['pe'].widget.attrs['readonly'] = True
        self.fields['ef'].widget.attrs['readonly'] = True
        self.fields['dx'].widget.attrs['readonly'] = True
        self.fields['plan'].widget.attrs['readonly'] = True
#Finalizan Formularios de CitaMedica

# Inician Formularios para lotes de Medicamentos
class LoteForm(forms.ModelForm):


    class Meta:
        model=LoteMedicamento
        fields=['fecha_vencimiento','existencias']

        Layout = Layout(Fieldset(' Agregar Lote:',
                                 Row('fecha_vencimiento'), Row('existencias')))

# Finalizan Formularios para lotes de Medicamentos

# Inician Formularios para Resultados de Examenes
class ResultadoForm(forms.ModelForm):

    class Meta:
        model=Resultado
        fields=[ 'examen','fecha_realizacion','lectura','comentario',]
        widgets = {

            'examen': forms.Select()
        }
        layout = Layout(Fieldset('Resultados de Examen:',
                                 Row('lectura','comentario',
                                     'fecha_realizacion','examen')))

    def __init__(self, *args, **kwargs):
        super(ResultadoForm, self).__init__(*args, **kwargs)
        self.fields['examen'].widget = forms.Select(choices=[(x.id, x.nombre) for x in Examen.objects.all()])
# Finalizan Formularios para Resultados de Examenes

# Inician Formularios para Planes de Tratamiento
class RecetaForm(forms.ModelForm):
    class Meta:
        model = Receta
        fields = ['indicaciones',]

class DosisForm(forms.ModelForm):
    class Meta:
        model = Dosis
        fields = ('cantidad', 'dosis', 'medicamento',)
# Finalizan Formularios para Planes de Tratamiento
