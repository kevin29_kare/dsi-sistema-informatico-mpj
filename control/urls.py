"""
Nombre del Módulo: Archivo de URLs para la app “control”
Autores: Francisco Rivas, Kevin Rodríguez, Irving Oliva, Fernando Rivas
Última fecha de modificación: 04/10/2017
"""

from django.conf.urls import url
from . import views

urlpatterns = [
    # Inician URLs de Citas Medicas
    url(r'^agregar_cita/$', views.AgregarCita.as_view(), name='agregar_cita'),
    url(r'^agenda_citas/$', views.agenda_citas, name='agenda_citas'),
    url(r'^citamedica_confirmar/(?P<pk>\d+)$', views.EliminarCitaMedica.as_view(),name='EliminarCitaMedica'),
    url(r'^citamedica_actualizar/(?P<pk>\d+)$', views.ActualizarCitaMedica.as_view(), name='ActualizarCitaMedica'),
    url(r'^agregar_puntos_vitales/(?P<pk>\d+)$', views.PuntosVitales.as_view(), name='PuntosVitales'),
    url(r'^agregar_diagnostico/(?P<pk>\d+)$', views.Diagnostico.as_view(), name='Diagnostico'),
    url(r'^consultar_diagnostico/(?P<pk>\d+)$', views.ConsultarDiagnostico.as_view(), name='ConsultarDiagnostico'),
    # Finalizan URLs de Citas Medicas

    # Inician URLs de Medicamentos
    url(r'^agregar_medicamento/$', views.AgregarMedicamento.as_view(), name='agregar_medicamento'),
    url(r'^medicamentos/$', views.consultarMedicamento, name='consultarMedicamento'),
    url(r'^actualizar_medicamento/(?P<pk>\d+)$', views.ModificarMedicamento.as_view(),name='MedicamentoUpdate'),
    url(r'^desactivar_medicamento/(?P<medicamento_id>[0-9]+)$', views.desactivar_medicamento, name='desactivar_medicamento'),
    url(r'^confirmar_desactivar_medicamento/(?P<medicamento_id>[0-9]+)$', views.confirmar_desactivar_medicamento, name='confirmar_desactivar_medicamento'),
    # Finalizan URLs de Medicamentos

    # Inician URLs de Examenes
    url(r'^agregar_examen/$', views.AgregarExamen.as_view(), name='agregar_examen'),
    url(r'^examenes/$', views.consultarExamen, name='consultarExamen'),
    url(r'^actualizar_examen/(?P<pk>\d+)$', views.ModificarExamen.as_view(),name='ExamenUpdate'),
    url(r'^desactivar_examen/(?P<examen_id>[0-9]+)$', views.desactivar_examen, name='desactivar_examen'),
    url(r'^confirmar_desactivar_examen/(?P<examen_id>[0-9]+)$', views.confirmar_desactivar_examen, name='confirmar_desactivar_examen'),
    # Finalizan URLs de Examenes

    #Inician URLs de Lotes de Medicamentos
    url(r'^agregar_lotes/(?P<medicamento_id>[0-9]+)$', views.Agregar_lotes, name='agregar_lotes'),
    url(r'^lotes/(?P<medicamento_id>[0-9]+)$', views.consultarLotes, name='consultarLotes'),
    url(r'^actualizar_lote/(?P<lote_id>[0-9]+)$', views.LoteUpdate, name='actualizar_lote'),
    # Finalizan URLs de Lotes de Medicamentos

    # Inician URLs de Resultados de Examenes
    url(r'^agregar_resultado/(?P<exp_id>[0-9]+)$', views.AgregarResultado, name='agregar_resultado'),
    url(r'^consulta_lectura/(?P<exp_id>[0-9]+)$', views.consultarResultados, name='consultarResultados'),
    url(r'^detalle_resultado/(?P<pk>\d+)$', views.DetalleResultado.as_view(), name='Detalle_Resultado'),
    # Finalizan URLs de Resulados de Examenes

    # Inician URLs de Planes de Tratamientos
    url(r'^agregar_plan/(?P<expediente_id>\d+)$', views.PlanTratamientoCreate.as_view(), name='agregarPlan'),
    url(r'^detalle_plan/(?P<pk>\d+)/$', views.detallePlanes, name='detallePlan'),
    url(r'^imprimir_receta/(?P<pk>\d+)$', views.ImprimirRecetaPDFView.as_view(), name='imprimir_receta'),
    url(r'^agregar_receta/(?P<pt_id>\d+)$', views.RecetaCreateView.as_view(), name='agregarReceta'),
    url(r'^detalle_dosis/(?P<pk>\d+)$', views.detalleDosis, name='detalleDosis'),
    url(r'^agregar_dosis/(?P<r_id>\d+)$', views.DosisCreateView.as_view(), name='agregarDosis'),
    url(r'^actualizar_plan/(?P<pk>\d+)$', views.PlanTratamientoUpdateView.as_view(), name='actualizarPlan'),
    # Finalizan URLs de Planes de Tratamientos

    url(r'^agregar_diagnosticos/(?P<pk>\d+)$', views.Diagnostico2.as_view(), name='Diagnostico2'),
    url(r'^agregar_receta_diag/(?P<pt_id>\d+)$', views.AgregarRecetaDiagnostico, name='agregarRecetaDiagnostico2'),
]