"""# -*- coding: utf8 -*-"""
from django.db import models
from django.core.validators import MinValueValidator
from administracion.models import Paciente
from control.models import CHOICES, LoteMedicamento, Medicamento
from datetime import date

# Create your models here.

BANCOS = (('1', 'Banco Agricola'),('2', 'Banco Scotiabank'))

class Institucion(models.Model):
    nombre_institucion= models.CharField('Nombre de la Institución', max_length = 128)
    direccion= models.CharField('Dirección de la Institución', max_length=256, blank=False,null=False)
    nit_institucion = models.CharField('Número de NIT', max_length=17, blank=True, null=False, help_text='Formato: XXXX-XXXXXX-XXX-X', unique=True)
    telefono_institucion = models.CharField('Número de teléfono', max_length=9, help_text='Formato: XXXX-XXXX', blank=True, null=True, unique=True)

    def __str__(self):
        return self.nombre_institucion

    class Meta:
        ordering = ['nombre_institucion']
        verbose_name = 'Institucion'
        verbose_name_plural = 'Instituciones'

class Donador(models.Model):
    nombre = models.CharField('Nombres del donador', max_length=40, blank=False, null=False)
    apellido = models.CharField('Apellidos del donador', max_length=40, blank=False, null=False)
    telefono = models.CharField('Número de teléfono', max_length=9, help_text='Formato: XXXX-XXXX', blank=False, null=False, unique=True)
    email = models.EmailField('Correo Electrónico', max_length=254, blank=True, null=True, unique=True)
    nit = models.CharField('Número de NIT', max_length=17, blank=False, null=False, help_text='Formato: XXXX-XXXXXX-XXX-X', unique=True)
    institucion = models.OneToOneField(Institucion, help_text='Institución a la que pertenece el Donador', blank=True, null=True)
    activo = models.BooleanField('¿Activo?', default=True)
    def __str__(self):
        return '{} {}'.format(self.nombre, self.apellido)

    class Meta:
        ordering = ['apellido']
        verbose_name = 'Donador'
        verbose_name_plural = 'Donadores'

class BienMueble(models.Model):
    nombre = models.CharField('Nombre del Bien Mueble', max_length=40, blank=False, null=False, unique = True)
    stock = models.IntegerField('Existencias Totales', blank = False, null = False, default = 0, validators = [MinValueValidator(0)])
    estado  = models.CharField('Estado', max_length = 1, choices = CHOICES, blank = False, null = False,default = ('1'))


    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ['nombre']
        verbose_name = 'Bien Mueble'
        verbose_name_plural = 'Bienes Muebles'

class LoteBienMueble(models.Model):
    fecha_ingreso = models.DateTimeField('Fecha de Ingreso', blank=False, null=False, auto_now_add=True)
    cantidad_ingreso = models.IntegerField('Cantidad o Monto inicial', blank=False, null=False, validators=[MinValueValidator(0)])
    existencias = models.IntegerField('Existencias del Lote', blank=False, null=False, validators=[MinValueValidator(0)])
    bien_mueble = models.ForeignKey(BienMueble, blank=False, null=False)
    estado = models.CharField('estado', max_length=1, choices=CHOICES, blank=False, null=False, default=('1'))

    def __str__(self):
        return "Lote de " + self.bien_mueble.nombre

    class Meta:
        ordering = ["bien_mueble", "fecha_ingreso"]
        verbose_name = 'Lote de Bien Mueble'
        verbose_name_plural = 'Lotes de Bien Mueble'

class Donacion(models.Model):
    fecha_ingreso  = models.DateField('Fecha de Ingreso', blank=False, null=False, default=date.today)
    cantidad = models.IntegerField(help_text = 'Cantidad o Monto Donado', blank=False, null=False, validators=[MinValueValidator(0)])
    monto_inicial = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    descripcion = models.CharField(help_text = 'Descripción de la Donación', max_length=512, blank=False, null=False)
    donador = models.ForeignKey(Donador, blank=False, null=False)
    fecha_remesa = models.DateField('Fecha de Remesa', help_text='Formato: DD/MM/AAAA', blank=True, null=True)
    lote_medicamento = models.OneToOneField(LoteMedicamento, blank=True, null=True)
    lote_bienmueble = models.OneToOneField(LoteBienMueble, blank=True, null=True)
    banco = models.CharField(max_length=1, choices=BANCOS, blank=True, null=True)
    comprobante = models.BooleanField('¿Solicitar Comprobante?', default=False)

    def __str__(self):
        return "Donación de " + self.donador.nombre

    class Meta:
        ordering = ["donador"]
        verbose_name = 'Donación'
        verbose_name_plural = 'Donaciones'

class Salida(models.Model):
    fecha = models.DateField('Fecha de Salida')
    ingreso_medicamento = models.DecimalField('Ingreso por medicamentos ($)', max_digits = 8, decimal_places = 2, blank = True, null = True, validators = [MinValueValidator(0)])
    concepto = models.CharField('En concepto de', max_length=1024, blank=False, null=True)
    paciente = models.ForeignKey(Paciente, null=True, blank=True, help_text='Paciente')
    donacion = models.ManyToManyField(Donacion, blank=True, help_text='Donacion')

    def __str__(self):
        return '{}: {}'.format(self.fecha, self.concepto)

    class Meta:
        verbose_name = 'Salida'
        verbose_name_plural = 'Salidas'

class DetalleSalida(models.Model):
    cantidad = models.IntegerField(help_text='Cantidad a donar', blank=False, null=False, validators=[MinValueValidator(0)])
    salida = models.ForeignKey(Salida, null=False, blank=False)
    medicamento = models.ForeignKey(Medicamento, null=True, blank=True, help_text='Medicamento')
    bienmueble = models.ForeignKey(BienMueble, null=True, blank=True, help_text='Bien Mueble')

    class Meta:
        verbose_name = 'Detalle de Salida'
        verbose_name_plural = 'Detalles de Salida'

class Procedimiento(models.Model):
    fecha  = models.DateField('Fecha de Ingreso', blank=False, null=False, default=date.today)
    monto = models.IntegerField(validators=[MinValueValidator(0)], null=False, blank=False)
    descripcion = models.CharField(help_text = 'Concepto del Procedimiento', max_length=512, blank=False, null=False)
    paciente = models.ForeignKey(Paciente, blank=False, null=False)

    def __str__(self):
        return "Donación en concepto de " + self.descripcion
