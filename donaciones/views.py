"""
Nombre del Módulo: Archivo de vistas para la app “donaciones”
Autores: Francisco Rivas, Kevin Rodríguez, Fernando Rivas, Irving Oliva, Martin Valle
Última fecha de modificación: 02/11/2017
"""

from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.utils import timezone
from django.urls import reverse_lazy, reverse
from django.shortcuts import render, redirect, HttpResponseRedirect
from .models import *
from django import  forms
from .forms import *
import datetime
# Create your views here.

class registroDonacion(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = Donacion
    fields = ['donador', 'cantidad', 'descripcion', 'fecha_remesa', 'banco', 'comprobante']
    template_name = 'donaciones/registro_donacion.html'
    success_url = reverse_lazy('agregar_donacion')
    success_message = "Donacion registrada con Exito."
    permission_required = 'donaciones.add_donacion'

    layout = Layout(Fieldset('Registrar Donación:'), Row('donador', 'cantidad'), Row('descripcion'), Row('fecha_remesa', 'banco'), Row('comprobante'))

    def form_valid(self, form):
        self.object = form.save()
        self.object.monto_inicial = self.object.cantidad
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

@login_required
def consultarDonaciones(request):
    if request.user.has_perm('donaciones.change_donacion') or request.user.has_perm('donaciones.delete_donacion'):
        donaciones_list = Donacion.objects.all()
        return render(request, 'donaciones/consultaDonaciones.html', {'donaciones_list': donaciones_list, })
    else:
        return HttpResponseRedirect('/base/')

class DetalleDonacion(PermissionRequiredMixin, UpdateView):
    model = Donacion
    form_class = DonacionForm
    template_name = 'donaciones/detalle_donacion.html'
    permission_required = 'donaciones.add_donacion, donaciones.change_donacion, donaciones.delete_donacion'

    def get_context_data(self, **kwargs):
        context = super(DetalleDonacion, self).get_context_data(**kwargs)
        context['tipo'] = 1
        return context

    def get_success_url(self):
        return reverse("donaciones")

@login_required
@permission_required('donaciones.change_donacion')
def ModificarDonacionDinero(request, id):
    donacion = Donacion.objects.get(pk=id)
    tipo = 2
    remesado = 0
    mensaje = " "
    if donacion.fecha_remesa:
        form = DonacionDineroForm2(request.POST or None, instance=donacion)
        remesado = 1
    else:
        form = DonacionDineroForm(request.POST or None, instance=donacion)
    if form.is_valid():
        mensaje = "Donacion Actualizada con Éxito"
    form2 = NombreDonadorForm(request.POST or None, instance=donacion)
    if form.is_valid():
        banco = form.cleaned_data['banco']
        remesado = form.cleaned_data['fecha_remesa']
        if banco and remesado:
            form.save()
        else:
            mensaje = "Complete Fecha de Remesa y/o Banco al Cual se Remeso"
            return redirect(reverse_lazy('actualizar_donacion', kwargs={'id': donacion.id}), mensaje)
        return redirect(reverse_lazy('consultarDonaciones',))
    return render(request, 'donaciones/detalle_donacion.html', {'form': form, 'tipo':tipo, 'remesado':remesado, 'form2':form2, 'mensaje': mensaje})

# Inician vistas para Donadores
@login_required
@permission_required('donaciones.add_donador')
def donadoresRegistar(request):
    mensaje=""
    Ins = None

    if request.method=='GET':
        donadores_form=DonadorForm(prefix='donador')
        institucion_form=InstitucionForm(prefix='institucion')

    #Cuando es POST
    if request.method=='POST':
        donadores_form=DonadorForm(request.POST or None, prefix='donador')
        institucion_form=InstitucionForm(request.POST or None, prefix='institucion')
        if donadores_form.is_valid():



            if institucion_form.is_valid():
                #Registro Institucion
                tnom= institucion_form.cleaned_data['nombre_institucion']
                tdireccion = institucion_form.cleaned_data['direccion']
                ttele = institucion_form.cleaned_data['telefono_institucion']
                tnit = institucion_form.cleaned_data['nit_institucion']

                Ins=Institucion.objects.create(

                    nombre_institucion=tnom,
                    direccion=tdireccion,
                    telefono_institucion=ttele,
                    nit_institucion = tnit
                )


            tnombre= donadores_form.cleaned_data['nombre']
            tapellido= donadores_form.cleaned_data['apellido']
            ttelefono = donadores_form.cleaned_data['telefono']
            tcorreo= donadores_form.cleaned_data['email']
            tnumero_nit = donadores_form.cleaned_data['nit']

            Donadores=Donador.objects.create(
                nombre=tnombre,
                apellido=tapellido,
                telefono=ttelefono,
                email=tcorreo,
                nit = tnumero_nit,
                institucion=Ins
            )
            #Limpiando campos despues de guardar
            mensaje="Registro realizado con exito"
            donadores_form=DonadorForm(prefix='donador')
            institucion_form=InstitucionForm(prefix='institucion')

    extra_context = {
        'donadores_form':donadores_form,
        'institucion_form': institucion_form,
        'mensaje': mensaje
    }

    return render(request, 'donaciones/agregar_donadores.html', extra_context)

@login_required
@permission_required('donaciones.change_donador')
def actualizarDonador(request,id):
    mensaje=""
    donadores = Donador.objects.get(pk=id)
    organizacion=donadores.institucion

    institucion_form = InstitucionForm(data=request.POST or None, instance=organizacion,prefix='institucion')
    donadores_form = DonadorForm(data=request.POST or None, instance=donadores,prefix='donador')


    if request.POST:
        if donadores_form.is_valid():



            if institucion_form.is_valid():
                if organizacion is None:
                    tnom = institucion_form.cleaned_data['nombre_institucion']
                    tdireccion = institucion_form.cleaned_data['direccion']
                    ttele = institucion_form.cleaned_data['telefono_institucion']
                    tnit = institucion_form.cleaned_data['nit_institucion']

                    Ins = Institucion.objects.create(

                        nombre_institucion=tnom,
                        direccion=tdireccion,
                        telefono_institucion=ttele,
                        nit_institucion = tnit
                    )

                    donadores.institucion=Ins
                    donadores.save()
                    donadores_form.save()
                    mensaje = "Actualizacion Exitosa"
                    return redirect(reverse_lazy('Consultar_donaciones', ))
                else:

                    institucion_form.save()
                    donadores_form.save()
                    mensaje = "Actualizacion Exitosa"
                    return redirect(reverse_lazy('Consultar_donaciones', ))

            else:
                if organizacion is None:
                    institucion_form = InstitucionForm()
                    donadores_form.save()
                    mensaje = "Actualizacion Exitosa"
                    return redirect(reverse_lazy('Consultar_donaciones', ))
                else:
                    Mensaje="Error En registro de Institucion"
        else:
            mensaje="Error en el Registro del Donador"



    return render(request, 'donaciones/actualizar_donadores.html', {'donadores_form': donadores_form,
                                                                    'institucion_form': institucion_form,'mensaje': mensaje, })


@permission_required('donaciones.delete_donador')
@login_required
def confirmar_desactivar_donador(request, donador_id):
    donador = Donador.objects.get(pk=donador_id)
    mensaje = ""
    if donador.activo:
        mensaje = "desactivar"
    else:
        mensaje = "activar"

    return render(request, 'donaciones/desactivar_donador.html', {'donador': donador, 'mensaje': mensaje, })

@login_required
@permission_required('donaciones.delete_donador')
def desactivar_donador(request, donador_id):
    donador = Donador.objects.get(pk=donador_id)
    if donador.activo:
        donador.activo = 0
    else:
        donador.activo = 1

    donador.save()
    return HttpResponseRedirect('/donadores')

@login_required
def consultarDonadores(request):
    if request.user.has_perm('donaciones.change_donador') or request.user.has_perm(
            'donaciones.delete_donador'):
        donadores_list = Donador.objects.all()
        return render(request, 'donaciones/consultar_donadores.html', {'donadores_list': donadores_list, })
    else:
        return HttpResponseRedirect('/base')

# Inician vistas de Bienes Muebles
class AgregarBienMueble(SuccessMessageMixin, PermissionRequiredMixin,CreateView):
    model = BienMueble
    fields = ['nombre']
    template_name = 'donaciones/agregar_bien_mueble.html'
    success_message = "Bien Mueble Agregado con Éxito"
    permission_required = 'donaciones.add_bienmueble'
    success_url = reverse_lazy('agregar_bien_mueble')

class ModificarBienMueble(SuccessMessageMixin, PermissionRequiredMixin,UpdateView):
    model = BienMueble
    fields = ['nombre']
    template_name = 'donaciones/agregar_bien_mueble.html'
    success_message = "Bien Mueble Modificado con Éxito"
    permission_required = 'donaciones.change_bienmueble'

    def get_context_data(self, **kwargs):
        context = super(ModificarBienMueble, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("consultarBienMueble")


@login_required
def consultarBienMueble(request):
    if request.user.has_perm('donaciones.change_bienmueble') or request.user.has_perm('donaciones.delete_bienmueble'):
        bien_muebles_list = BienMueble.objects.all()
        return render(request, 'donaciones/consultar_bien_mueble.html', {'bien_muebles_list': bien_muebles_list, })
    else:
        return HttpResponseRedirect('/base/')

@login_required
@permission_required('donaciones.delete_bienmueble')
def desactivar_bien_mueble(request, bien_mueble_id):
    bien_mueble = BienMueble.objects.get(pk=bien_mueble_id)

    if bien_mueble.estado == '1':
        bien_mueble.estado = '0'
    else:
        bien_mueble.estado = '1'
    bien_mueble.save()
    return HttpResponseRedirect('/bien_muebles')

@permission_required('donaciones.delete_bienmueble')
@login_required
def confirmar_desactivar_bien_mueble(request, bien_mueble_id):
    bien_mueble=BienMueble.objects.get(pk=bien_mueble_id)
    mensaje=""
    if bien_mueble.estado == '1':
        mensaje="desactivar"
    else:
        mensaje="activar"

    return render(request, 'donaciones/modificar_bien_mueble.html', {'bien_mueble': bien_mueble, 'mensaje': mensaje, })
# Finalizan vistas de Bienes Muebles

# Inician Vistas de Lote de Bienes Muebles
@login_required
@permission_required('donaciones.add_lotebienmueble')
def Agregar_lote_muebles(request,bien_mueble_id):
    mensaje=""
    if request.method=='GET':
        lote_mueble_form=LoteForm(prefix='lote_mueble')

    #Cuando es POST
    if request.method=='POST':
        lote_mueble_form=LoteForm(request.POST or None, prefix='lote_mueble')
        if lote_mueble_form.is_valid():
            bien_mueble=BienMueble.objects.get(pk=bien_mueble_id)

            texitencia= int (lote_mueble_form.cleaned_data['existencias'])

            lote_mueble=LoteBienMueble.objects.create(
                existencias=texitencia,
                bien_mueble=bien_mueble,
                cantidad_ingreso=texitencia,
                estado=1
            )
            #Limpiando campos despues de guardar
            bien_mueble.stock = bien_mueble.stock + texitencia
            bien_mueble.save()
            mensaje="Lote Creado con exito"
            lote_mueble_form=LoteForm(prefix='lote_mueble')


    extra_context = {
        'lote_mueble_form':lote_mueble_form,
        'mensaje': mensaje,
        'bien_mueble_id':bien_mueble_id
    }

    return render(request, 'donaciones/agregar_lote_muebles.html', extra_context)




@login_required
def consultarLoteMuebles(request,bien_mueble_id):
    if request.user.has_perm('donaciones.change_lotebienmueble') or request.user.has_perm('donaciones.delete_lotebienmueble'):
        lote_muebles_list = LoteBienMueble.objects.filter(bien_mueble_id=bien_mueble_id ).filter(estado=1)
        return render(request, 'donaciones/consulta_lote_muebles.html', {'lote_muebles_list': lote_muebles_list, })
    else:
        return HttpResponseRedirect('/login/?next=/lote_muebles/')


@login_required
@permission_required('donaciones.change_lotebienmueble')
def LoteMuebleUpdate(request,lote_mueble_id ):
    mensaje = ""

    lote_mueble=LoteBienMueble.objects.get(pk=lote_mueble_id)
    bien_mueble2=lote_mueble.bien_mueble
    lote_mueble_inicial=lote_mueble.existencias

    if request.POST:
        if request.POST.get('existencias'):

            lote_mueble_entrante=int(request.POST.get('existencias'))
            lote_mueble.existencias=lote_mueble_entrante

            bien_mueble = BienMueble.objects.get(pk=lote_mueble.bien_mueble.id)
            diferencia = lote_mueble_entrante-lote_mueble_inicial

            if (lote_mueble_entrante==0):
                lote_mueble.estado=0
            if (diferencia>0):
                lote_mueble.cantidad_ingreso=lote_mueble.cantidad_ingreso+diferencia


            try:

                bien_mueble.stock=bien_mueble.stock+diferencia
                lote_mueble.save()
                bien_mueble.save()
                mensaje = "Lote Modificado con exito"
                bien_mueble2 =LoteBienMueble.objects.get(pk=lote_mueble_id).bien_mueble
            except:
                mensaje = "Error en formato de Datos"

        else:
            mensaje = "Error al Modificar  Lote de Bien Mueble"

    return render(request, 'donaciones/actualizar_lote_muebles.html', {'mensaje': mensaje, 'lote_mueble_id': lote_mueble_id, 'existencias': lote_mueble.existencias, 'bien_mueble':bien_mueble2.id, })

@permission_required('donaciones.add_bienmueble')
def donar_mueble(request):
    mensaje = ""
    donacion = None

    # Cuando es GET
    if request.method == 'GET':
        mueble_form = BienMuebleForm(prefix='mueble')
        donarmueble_form = DonacionModForm(prefix='donacion')

    # Cuando es POST
    if request.method == 'POST':
        mueble_form = BienMuebleForm(request.POST or None, prefix='mueble')
        donarmueble_form = DonacionModForm(request.POST or None, prefix='donacion')


        if mueble_form.is_valid() and donarmueble_form.is_valid():
            donador = donarmueble_form.cleaned_data['donador']
            cantidad = int (donarmueble_form.cleaned_data['cantidad'])
            descripcion = donarmueble_form.cleaned_data['descripcion']
            comprobante = donarmueble_form.cleaned_data['comprobante']

            bien = mueble_form.cleaned_data['bien_mueble']
            #stock = int (mueble_form.cleaned_data['stock'])

            #bien_mueble = BienMueble.objects.get(nombre=bien)


            nLote = LoteBienMueble.objects.create(
                existencias= cantidad,
                bien_mueble=bien,
                cantidad_ingreso=cantidad,
                estado=1
            )

            nDonacion = Donacion.objects.create(
                donador=donador,
                cantidad=cantidad,
                monto_inicial=cantidad,
                descripcion=descripcion,
                lote_bienmueble=nLote,
                comprobante=comprobante
            )

            bien.stock = bien.stock + cantidad
            bien.save()

            mensaje = "Donacion Creada con Exito"
            # Limpiando campos después de guardar (Reset Forms)
            mueble_form = BienMuebleForm(prefix='mueble')
            donarmueble_form = DonacionModForm(prefix='donacion')


    extra_context = {
        'mueble_form': mueble_form,
        'donarmueble_form': donarmueble_form,
        'mensaje': mensaje
    }

    return render(request, 'donaciones/agregar_donacion_bienmueble.html', extra_context)
# Finalizan vistas de Lotes de Bienes Muebles

# Incian vistas de Donaciones de medicamentos

@permission_required('donaciones.add_donacion')
def Donar_lote(request):
    mensaje = ""
    donacion = None

    # Cuando es GET
    if request.method == 'GET':
        lote_form = LoteMediForm(prefix='lote_donacion')
        donacion_form = DonacionModForm(prefix='donacion')

    # Cuando es POST
    if request.method == 'POST':
        lote_form = LoteMediForm(request.POST or None, prefix='lote_donacion')
        donacion_form = DonacionModForm(request.POST or None, prefix='donacion')


        if lote_form.is_valid() and donacion_form.is_valid():
            donador = donacion_form.cleaned_data['donador']
            cantidad = int (donacion_form.cleaned_data['cantidad'])
            descripcion = donacion_form.cleaned_data['descripcion']
            comprobante = donacion_form.cleaned_data['comprobante']

            tmedicamento = lote_form.cleaned_data['medicamento']
            tfecha=lote_form.cleaned_data['fecha_vencimiento']


            nLote = LoteMedicamento.objects.create(
                existencias= cantidad,
                cantidad_ingreso=cantidad,
                fecha_vencimiento=tfecha,
                medicamento=tmedicamento,

            )

            nDonacion = Donacion.objects.create(
                donador=donador,
                cantidad=cantidad,
                monto_inicial=cantidad,
                descripcion=descripcion,
                lote_medicamento=nLote,
                comprobante=comprobante
            )

            tmedicamento.stock = tmedicamento.stock + cantidad
            tmedicamento.save()

            mensaje = "Donacion Creada con Exito"
            # Limpiando campos después de guardar (Reset Forms)
            lote_form = LoteMediForm(prefix='lote_donacion')
            donacion_form = DonacionModForm(prefix='donacion')


    extra_context = {
        'lote_form': lote_form,
        'donacion_form': donacion_form,
        'mensaje': mensaje
    }

    return render(request, 'donaciones/agregar_donacion_lotemedicamento.html', extra_context)

class DetalleLoteDonacion(PermissionRequiredMixin, UpdateView):
    model = Donacion
    form_class = DetalleLote
    template_name = 'donaciones/Detalle_lote.html'
    permission_required = 'donaciones.add_donacion, donaciones.change_donacion, donaciones.delete_donacion'


    def get_success_url(self):
        return reverse("consultarDonaciones")

#Empiezam views de salidas

@permission_required('donaciones.add_salida')
def SalidaEfectivo(request):
    mensaje = ""
    donacion = None

    # Cuando es GET
    if request.method == 'GET':
      salida_form=SalidaFrom(prefix='salida')
      detalle_form = DetalleForm(prefix='detalle')

    # Cuando es POST
    if request.method == 'POST':
        salida_form = SalidaFrom(request.POST or None, prefix='salida')
        detalle_form = DetalleForm(request.POST or None,prefix='detalle')
        if salida_form.is_valid() & detalle_form.is_valid():
          tconcepto=salida_form.cleaned_data['concepto']
          tcantidad= int(detalle_form.cleaned_data['cantidad'])
          donaciones=Donacion.objects.filter(lote_bienmueble__isnull=True, lote_medicamento__isnull=True, cantidad__gt=0,fecha_remesa__isnull=False).order_by('fecha_remesa')
          total = 0

          for donas in donaciones:
             total=total+donas.cantidad

          # Se valida que exista mas efectivo que el que se solicita
          if (total>=tcantidad):
            tsalida = Salida.objects.create(
              concepto=tconcepto
              )
            cant=0
            contador = 0
            provisional = tcantidad
            while(cant==0):

              donacion=donaciones.__getitem__(contador)

              if (donacion.cantidad>provisional):
                  donacion.cantidad=donacion.cantidad-provisional
                  donacion.save()
                  tsalida.donacion.add(donacion)
                  cant=1

              elif(donacion.cantidad<provisional):
                   provisional=provisional-donacion.cantidad
                   donacion.cantidad = 0
                   donacion.save()
                   tsalida.donacion.add(donacion)
                   contador+=1

              else:
                  donacion.cantidad=0
                  donacion.save()
                  tsalida.donacion.add(donacion)
                  cant=1


            ndetalle=DetalleSalida.objects.create(
                cantidad=tcantidad,
                salida=tsalida
            )

            salida_form = SalidaFrom(prefix='salida')
            detalle_form = DetalleForm(prefix='detalle')
            mensaje ="Registro Exitoso"

          else:
              mensaje="Cantidad mayor al Efectivo actual en Donaciones"

        else:
           mensaje="Datos Faltantes o Error en datos"


    extra_context = {
        'salida_form': salida_form,
        'detalle_form': detalle_form,
        'mensaje': mensaje
    }

    return render(request, 'donaciones/salida_efectivo.html', extra_context)

#Views de BIen mueble Donacion

@permission_required('donaciones.add_salida')
def SalidaBienMueble(request):
    mensaje = ""
    donacion = None

    # Cuando es GET
    if request.method == 'GET':
      salida_form=SalidaFrom(prefix='salida')
      detalle_form = DetalleForm(prefix='detalle')
      bien_form=SalidaBienMuebleForm(prefix='salida_bien')

    # Cuando es POST
    if request.method == 'POST':
        salida_form = SalidaFrom(request.POST or None, prefix='salida')
        detalle_form = DetalleForm(request.POST or None,prefix='detalle')
        bien_form = SalidaBienMuebleForm(request.POST or None,prefix='salida_bien')

        if salida_form.is_valid() & detalle_form.is_valid() & bien_form.is_valid():
          tconcepto=salida_form.cleaned_data['concepto']
          tcantidad= int(detalle_form.cleaned_data['cantidad'])
          tmueble=bien_form.cleaned_data['bien_mueble']

          donaciones=Donacion.objects.filter(lote_medicamento__isnull=True, cantidad__gt=0,lote_bienmueble__bien_mueble__nombre=tmueble.nombre,lote_bienmueble__bien_mueble__estado=1
                                             ,lote_bienmueble__existencias__gt=0).order_by('fecha_ingreso')
          total = 0

          for donas in donaciones:
             total=total+donas.cantidad

          # Se valida que exista mas bien mueble que el que se solicita
          if (total>=tcantidad):
            tsalida = Salida.objects.create(
              concepto=tconcepto

              )
            cant=0
            contador = 0
            provisional = tcantidad
            while(cant==0):

              donacion=donaciones.__getitem__(contador)

              if (donacion.cantidad>provisional):
                  donacion.cantidad=donacion.cantidad-provisional
                  donacion.save()
                  donacion.lote_bienmueble.existencias=donacion.lote_bienmueble.existencias-provisional
                  donacion.lote_bienmueble.save()
                  tmueble.stock=tmueble.stock-provisional
                  tmueble.save()
                  tsalida.donacion.add(donacion)
                  cant=1

              elif(donacion.cantidad<provisional):
                   provisional=provisional-donacion.cantidad
                   donacion.cantidad = 0
                   donacion.save()
                   tsalida.donacion.add(donacion)
                   donacion.lote_bienmueble.existencias = donacion.lote_bienmueble.existencias - provisional
                   donacion.lote_bienmueble.save()
                   tmueble.stock = tmueble.stock - provisional
                   tmueble.save()
                   contador+=1

              else:
                  donacion.cantidad=0
                  donacion.save()
                  tsalida.donacion.add(donacion)
                  donacion.lote_bienmueble.existencias = donacion.lote_bienmueble.existencias - provisional
                  donacion.lote_bienmueble.save()
                  tmueble.stock = tmueble.stock - provisional
                  tmueble.save()
                  cant=1


            ndetalle=DetalleSalida.objects.create(
                cantidad=tcantidad,
                salida=tsalida,
                bienmueble=tmueble
            )

            salida_form = SalidaFrom(prefix='salida')
            detalle_form = DetalleForm(prefix='detalle')
            bien_form = SalidaBienMuebleForm(prefix='salida_bien')
            mensaje ="Registro Exitoso"

          else:
              mensaje="Cantidad mayor al Bien Mueble "+ tmueble.nombre + " Disponibles"

        else:
           mensaje="Datos Faltantes o Error en datos"


    extra_context = {
        'salida_form': salida_form,
        'detalle_form': detalle_form,
        'bien_form': bien_form,
        'mensaje': mensaje
    }

    return render(request, 'donaciones/salida_bienmueble.html', extra_context)

@permission_required('donaciones.add_salida')
def salida_medicamentos(request):
    mensaje = ""
    pacientes = Paciente.objects.filter(activo=1)
    medicamentos = Medicamento.objects.filter(estado='1', stock__gt=0)
    fecha = datetime.datetime.today().strftime('%d/%m/%Y')
    if request.POST:
        medicamentos_id_selected = request.POST.getlist('medicamento[]')
        if medicamentos_id_selected.__len__() == set(medicamentos_id_selected).__len__():
            # Si no hay duplicados
            cantidad_selected = request.POST.getlist('cantidad[]')
            paciente = request.POST.get('paciente')
            ingreso_medicamento = request.POST.get('ingreso_medicamento')
            fecha_salida = request.POST.get('fecha')
            if not paciente:
                paciente = None
            salida = Salida.objects.create(fecha=datetime.datetime.strptime(fecha_salida, '%d/%m/%Y'), concepto=request.POST.get('salida-concepto'),
                                           paciente_id=paciente, ingreso_medicamento=ingreso_medicamento)
            for i, medicamento_id in enumerate(medicamentos_id_selected):
                medicamento = Medicamento.objects.get(pk=medicamento_id)
                lotes_medicamento = LoteMedicamento.objects.filter(medicamento_id=medicamento_id,
                                                                   existencias__gt=0).order_by('fecha_vencimiento')
                cantidad = int(cantidad_selected[i])
                for lote_medicamento in lotes_medicamento:
                    if cantidad > lote_medicamento.existencias:
                        cantidad = cantidad - lote_medicamento.existencias
                        lote_medicamento.existencias = 0
                        lote_medicamento.estado = 0
                        lote_medicamento.save()
                        try:
                            donacion = lote_medicamento.donacion
                            donacion.cantidad = 0
                            donacion.save()
                        except:
                            pass
                    else:
                        lote_medicamento.existencias = lote_medicamento.existencias - cantidad
                        if lote_medicamento.existencias == 0:
                            lote_medicamento.estado = 0
                        lote_medicamento.save()
                        try:
                            donacion = lote_medicamento.donacion
                            donacion.cantidad = donacion.cantidad - cantidad
                            donacion.save()
                        except:
                            pass
                        cantidad = 0
                        break;
                medicamento.stock = medicamento.stock - int(cantidad_selected[i])
                medicamento.save()
                DetalleSalida.objects.create(cantidad=int(cantidad_selected[i]), medicamento=medicamento, salida=salida)
            mensaje = "Salida de Medicamento registrada con éxito"

        else:
            # Si hay duplicados
            mensaje = "Se detectaron medicamentos duplicados, asegúrese de ingresar los medicamentos correctos"

    context = {
        'mensaje': mensaje,
        'pacientes': pacientes,
        'medicamentos': medicamentos,
        'fecha': fecha
    }
    return render(request, 'donaciones/salida_medicamentos.html', context)

class AgregarIngresoProcedimiento(SuccessMessageMixin, PermissionRequiredMixin,CreateView):
    model = Procedimiento
    fields = ['paciente','monto','descripcion']
    template_name = 'donaciones/agregar_procedimiento.html'
    success_message = "Procedimiento registrado con exito"
    permission_required = 'donaciones.add_procedimiento'
    success_url = reverse_lazy('agregar_procedimiento')

    def get_form(self):
        form = super(AgregarIngresoProcedimiento, self).get_form()
        form.fields['paciente'].widget = forms.Select(choices=[('', 'Paciente Externo')] + list((((x.id, x) for x in Paciente.objects.all()))))
        form.fields['paciente'].help_text = "Digite el Número de Expediente ó el nombre del paciente"
        return form

class ModificarIngresoProcedimiento(SuccessMessageMixin, PermissionRequiredMixin,UpdateView):
    model = Procedimiento
    fields = ['paciente','monto','descripcion']
    template_name = 'donaciones/agregar_procedimiento.html'
    success_message = "Registro Modificado con Éxito"
    permission_required = 'donaciones.change_procedimiento'

    def get_form(self):
        form = super(ModificarIngresoProcedimiento, self).get_form()
        form.fields['paciente'].widget = forms.Select(choices=[('', 'Paciente Externo')] + list((((x.id, x) for x in Paciente.objects.all()))))
        form.fields['paciente'].help_text = "Digite el Número de Expediente ó el nombre del paciente"
        return form

    def get_context_data(self, **kwargs):
        context = super(ModificarIngresoProcedimiento, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("consultarProcedimientos")

@login_required
def consultarProcedimientos(request):
    if request.user.has_perm('donaciones.change_procedimiento') or request.user.has_perm('donaciones.delete_procedimiento'):
        procedimientos_list = Procedimiento.objects.all()
        return render(request, 'donaciones/consultar_procedimientos.html', {'procedimientos_list': procedimientos_list, })
    else:
        return HttpResponseRedirect('/base/')


@login_required
def consultarProcedimientosPaciente(request, pac_id):
    procedimientos_lis = Procedimiento.objects.filter(paciente_id=pac_id)
    vista_paciente = True
    return render(request, 'donaciones/consultar_procedimientos.html', {'procedimientos_list': procedimientos_lis, 'vista_paciente': vista_paciente,})

