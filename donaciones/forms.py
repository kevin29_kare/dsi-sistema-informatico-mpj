
"""
Nombre del Módulo: Archivo de formularios para la app “donaciones”
Autores: Kevin Rodríguez, Irving Oliva, Fernando Rivas
Última fecha de modificación: 04/11/2017
"""
from django import forms
from django.contrib.auth.models import User
from material import *
from .models import *

# Inician Formularios de Donador
class DonadorForm(forms.ModelForm):
    class Meta:
        model=Donador
        fields=['nombre','apellido','telefono','email','nit']

    layout = Layout(Fieldset('Información del Donador:',
                             Row('nombre', 'apellido'), Row('telefono', 'email', 'nit')))

class InstitucionForm(forms.ModelForm):
    class Meta:
        model = Institucion
        fields = ['nombre_institucion', 'direccion', 'telefono_institucion', 'nit_institucion']

    layout = Layout(Fieldset('Información de Institucion:',
                             Row('nombre_institucion', 'telefono_institucion'),
                             Row('direccion', 'nit_institucion')))

# Finalizan Formularios de Donadors

# Inician Formularios de Donaciones
class DonacionForm(forms.ModelForm):
    class Meta:
        model = Donacion
        fields = ['fecha_ingreso', 'cantidad', 'descripcion', 'donador', 'lote_bienmueble','lote_medicamento']
    layout = Layout(Fieldset('Información de la Donación:'), Row('donador'),Row('lote_bienmueble', 'fecha_ingreso'), Row('cantidad', 'descripcion'))

    def __init__(self, *args, **kwargs):
        super(DonacionForm, self).__init__(*args, **kwargs)
        self.fields['donador'].widget.attrs['readonly'] = True
        self.fields['cantidad'].widget.attrs['readonly'] = True
        self.fields['descripcion'].widget.attrs['readonly'] = True
        self.fields['fecha_ingreso'].widget.attrs['readonly'] = True
        self.fields['lote_bienmueble'].widget.attrs['readonly'] = True


class NombreDonadorForm(forms.ModelForm):
    class Meta:
        model = Donacion
        fields = ['donador']
    layout = Layout(Fieldset('Información del Donador:'),Row('donador'))

    def __init__(self, *args, **kwargs):
        super(NombreDonadorForm, self).__init__(*args, **kwargs)
        self.fields['donador'].widget.attrs['readonly'] = True


class DonacionDineroForm(forms.ModelForm):
    class Meta:
        model = Donacion
        fields = ['fecha_ingreso', 'cantidad', 'descripcion','fecha_remesa', 'banco']
    layout = Layout(Fieldset('Información de la Donación:'), Row('cantidad', 'fecha_ingreso'), Row('descripcion'), Row('fecha_remesa', 'banco'))

    def __init__(self, *args, **kwargs):
        super(DonacionDineroForm, self).__init__(*args, **kwargs)
        self.fields['cantidad'].widget.attrs['readonly'] = True
        self.fields['descripcion'].widget.attrs['readonly'] = True
        self.fields['fecha_ingreso'].widget.attrs['readonly'] = True

class DonacionDineroForm2(forms.ModelForm):
    class Meta:
        model = Donacion
        fields = ['fecha_ingreso', 'cantidad', 'descripcion','fecha_remesa', 'banco']
    layout = Layout(Fieldset('Información de la Donación:'), Row('cantidad', 'fecha_ingreso'), Row('descripcion'), Row('fecha_remesa', 'banco'))

    def __init__(self, *args, **kwargs):
        super(DonacionDineroForm2, self).__init__(*args, **kwargs)
        self.fields['cantidad'].widget.attrs['readonly'] = True
        self.fields['descripcion'].widget.attrs['readonly'] = True
        self.fields['fecha_ingreso'].widget.attrs['readonly'] = True
        self.fields['fecha_remesa'].widget.attrs['readonly'] = True
        self.fields['banco'].widget.attrs['readonly'] = True

class DonacionModForm(forms.ModelForm):
    class Meta:
        model = Donacion
        fields = ['fecha_ingreso', 'cantidad', 'descripcion', 'donador', 'comprobante']

    layout = Layout(Fieldset('Información de la Donación:'), Row('donador', 'fecha_ingreso'),
                    Row('cantidad', 'descripcion'), Row('comprobante'))

    def __init__(self, *args, **kwargs):
        super(DonacionModForm, self).__init__(*args, **kwargs)
        self.fields['donador'].widget.attrs['readonly'] = True
        self.fields['fecha_ingreso'].widget.attrs['readonly'] = True

# Finalizan Formularios de Donaciones

# Inician Formularios para lotes de Bienes Muebles
class LoteForm(forms.ModelForm):


    class Meta:
        model=LoteBienMueble
        fields=['existencias']

        Layout = Layout(Fieldset(' Agregar Lote Bien Mueble:', Row('existencias')))

class BienMuebleForm(forms.ModelForm):
    class Meta:
        model = LoteBienMueble
        fields = ['bien_mueble']

    layout = Layout(Fieldset('Información del Bien Mueble:'), Row('bien_mueble'))

    def __init__(self, *args, **kwargs):
        super(BienMuebleForm, self).__init__(*args, **kwargs)
        self.fields['bien_mueble'].widget.attrs['readonly'] = True

# Finalizan Formularios para lotes de Bienes Muebles


# Inician Formularios para lotes de Medicamentos para Donaciones
class LoteMediForm(forms.ModelForm):
    class Meta:
        model = LoteMedicamento
        fields = ['medicamento','fecha_vencimiento']

    layout = Layout(Fieldset('Información del Lote De Medicamentos:'), Row('medicamento'),'fecha_vencimiento')

    def __init__(self, *args, **kwargs):
        super(LoteMediForm, self).__init__(*args, **kwargs)
        self.fields['medicamento'].widget.attrs['readonly'] = True


class DetalleLote(forms.ModelForm):
    class Meta:
        model = Donacion
        fields = ['fecha_ingreso', 'cantidad', 'descripcion', 'donador','lote_medicamento']
    layout = Layout(Fieldset('Información de la Donación:'), Row('donador'),Row('lote_medicamento', 'fecha_ingreso'), Row('cantidad', 'descripcion'))

    def __init__(self, *args, **kwargs):
        super(DetalleLote, self).__init__(*args, **kwargs)
        self.fields['donador'].widget.attrs['readonly'] = True
        self.fields['cantidad'].widget.attrs['readonly'] = True
        self.fields['descripcion'].widget.attrs['readonly'] = True
        self.fields['fecha_ingreso'].widget.attrs['readonly'] = True
        self.fields['lote_medicamento'].widget.attrs['readonly'] = True

# Finalizan Formularios para lotes de Medicamentos para Donaciones

# Inician los Formularios de Salida de Efectivo
class SalidaFrom(forms.ModelForm):
    class Meta:
        model = Salida
        fields = ['concepto']

    layout = Layout(Fieldset(' Concepto de la Salida:'), Row('concepto'))

class DetalleForm(forms.ModelForm):
    class Meta:
        model = DetalleSalida
        fields = ['cantidad']

    layout = Layout(Fieldset(' Cantidad de salida:'), Row('cantidad'))

# Finalizan los Formularios de Salida de Efectivo

# Inician los Formularios de Salida de Bien Mueble

class SalidaBienMuebleForm(forms.ModelForm):
        class Meta:
            model = LoteBienMueble
            fields = ['bien_mueble', ]

        layout = Layout(Fieldset('Informacion del Bien Mueble:'), Row('bien_mueble'))

        def __init__(self, *args, **kwargs):
            super(SalidaBienMuebleForm, self).__init__(*args, **kwargs)
            self.fields['bien_mueble'].widget.attrs['readonly'] = True

# Finalizan los Formularios de Salida de Bien Mueble