from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^agregar_donacion/$', views.registroDonacion.as_view(), name='agregar_donacion'),
    url(r'^donaciones/$', views.consultarDonaciones, name='consultarDonaciones'),
    url(r'^donaciones/(?P<pk>\d+)$', views.DetalleDonacion.as_view(), name='detalle_donacion'),
    url(r'^donar_bienmueble/$', views.donar_mueble, name='donar_bienmueble'),
    url(r'^actualizar_donacion/(?P<id>[0-9]+)$', views.ModificarDonacionDinero, name='actualizar_donacion'),
    #inician URLS de Donadores
    url(r'^agregar_donadores/$', views.donadoresRegistar, name='agregar_donadores'),
    url(r'^actualizar_donadores/(?P<id>[0-9]+)$', views.actualizarDonador, name='actualizar_donador'),
    url(r'^desactivar_donador/(?P<donador_id>[0-9]+)$', views.desactivar_donador, name='desactivar_donador'),
    url(r'^confirmar_desactivar/(?P<donador_id>[0-9]+)$', views.confirmar_desactivar_donador, name='confirmar'),
    url(r'^donadores/$', views.consultarDonadores, name='Consultar_donaciones'),
    
    # Inician URLs de BienMuebles
    url(r'^agregar_bien_mueble/$', views.AgregarBienMueble.as_view(), name='agregar_bien_mueble'),
    url(r'^bien_muebles/$', views.consultarBienMueble, name='consultarBienMueble'),
    url(r'^actualizar_bien_mueble/(?P<pk>\d+)$', views.ModificarBienMueble.as_view(),name='BienMuebleUpdate'),
    url(r'^desactivar_bien_mueble/(?P<bien_mueble_id>[0-9]+)$', views.desactivar_bien_mueble, name='desactivar_bien_mueble'),
    url(r'^confirmar_desactivar_bien_mueble/(?P<bien_mueble_id>[0-9]+)$', views.confirmar_desactivar_bien_mueble, name='confirmar_desactivar_bien_mueble'),
    # Finalizan URLs de BienMuebles

    # Inician URLs de Lotes de Bien Mueble
    url(r'^agregar_lote_mueble/(?P<bien_mueble_id>[0-9]+)$', views.Agregar_lote_muebles, name='agregar_lote_mueble'),
    url(r'^lotes_bienes_muebles/(?P<bien_mueble_id>[0-9]+)$', views.consultarLoteMuebles, name='consultarLoteMuebles'),
    url(r'^actualizar_lote_mueble/(?P<lote_mueble_id>[0-9]+)$', views.LoteMuebleUpdate, name='actualizar_lote_mueble'),
    # Finalizan URLs de Lotes de Bien Mueble

    #Inician URLS de Salidas
    url(r'^donar_medicamento/$', views.Donar_lote, name='donar_lotemedicamento'),
    url(r'^donaciones_lote/(?P<pk>\d+)$', views.DetalleLoteDonacion.as_view(), name='detalle_lotes'),
    url(r'^salida_efectivo/$', views.SalidaEfectivo, name='salida_efectivo'),
    url(r'^salida_bienmueble/$', views.SalidaBienMueble, name='salida_bienmueble'),
    url(r'^salida_medicamentos/$', views.salida_medicamentos, name='salida_medicamentos'),
    #Finalizan URLS de Salidas

    url(r'^agregar_procedimiento/$', views.AgregarIngresoProcedimiento.as_view(), name='agregar_procedimiento'),
    url(r'^actualizar_procedimiento/(?P<pk>\d+)$', views.ModificarIngresoProcedimiento.as_view(), name='ProcedimientoUpdate'),
    url(r'^procedimientos/$', views.consultarProcedimientos, name='consultarProcedimientos'),
    url(r'^procedimientos_paciente/(?P<pac_id>[0-9]+)$', views.consultarProcedimientosPaciente, name='procedimientos_paciente'),

]