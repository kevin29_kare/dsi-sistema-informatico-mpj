from django.contrib import admin 
from .models import *

# Register your models here.
admin.site.register(Institucion)
admin.site.register(Donador)
admin.site.register(BienMueble)
admin.site.register(LoteBienMueble)
admin.site.register(Donacion)
admin.site.register(Salida)
admin.site.register(Procedimiento)
