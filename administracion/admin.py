from django.contrib import admin
from .models import *
# Register your models here.

class PacienteAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">person_add</i>'
    list_display = ["nombre","apellido", "expediente"]
    fieldsets = (
        ('Datos Generales', {
            'fields': (('nombre', 'apellido'), ('dui', 'nit'), ('fecha_nacimiento', 'telefono', 'direccion'), ('sexo', 'estado_civil'))
        }),

    )

    search_fields = ["nombre", "apellido"]

    class Meta:
        model = Paciente

class ResponsableAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">person_add</i>'

class EmpleadoAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">person_add</i>'
    list_display = ["nombre", "apellido", "activo"]

class ExpedienteAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">description</i>'
    list_display = ["num_expediente", "paciente", "fecha_apertura"]
    list_filter = ["fecha_apertura"]
    search_fields = ["num_expediente"]

    class Meta:
        model = Expediente

class EstadoCivilAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">supervisor_account</i>'
    list_display = ["nombre"]

    class Meta:
        model = EstadoCivil

admin.site.register(Paciente, PacienteAdmin)
admin.site.register(Expediente, ExpedienteAdmin)
admin.site.register(Empleado, EmpleadoAdmin)
admin.site.register(Responsable, ResponsableAdmin)
admin.site.register(EstadoCivil, EstadoCivilAdmin)
admin.site.register(Especialidad)
admin.site.register(Socioeconomico)
admin.site.register(Servicio)
admin.site.register(Participante)
admin.site.register(GrupoApoyo)
admin.site.register(Tarifa)
admin.site.register(Departamento)
admin.site.register(Municipio)
