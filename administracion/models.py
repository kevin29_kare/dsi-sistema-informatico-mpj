# -*- coding: utf-8 -*-
"""
Nombre del Módulo: Archivo de modelos para la app “administracion”
Autores: Francisco Rivas, Kevin Rodríguez, Fernando Rivas, Irving Oliva
Última fecha de modificación: 04/10/2017
"""
from django.db import models
from django.contrib.auth.models import User
from django.shortcuts import reverse
from django.core.validators import MinValueValidator

# Create your models here.

SEXO_CHOICES = (
    ('M', 'Masculino'),
    ('F', 'Femenino'),
)

GRUPO_CHOICES = (('1', 'Programado'),('2', 'Finalizado'))

class Departamento(models.Model):
    nombre = models.CharField(max_length=32, blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name='Departamento'
        verbose_name_plural = 'Departamentos'


class Municipio(models.Model):
    nombre = models.CharField(max_length=64, blank=False, null=False)
    departamento = models.ForeignKey(Departamento, blank=False, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name = 'Municipio'
        verbose_name_plural = 'Municipios'

class EstadoCivil(models.Model):
    nombre = models.CharField(max_length = 30,unique=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name='Estado Civil'
        verbose_name_plural = 'Estados Civiles'

class Especialidad(models.Model):
    nombre = models.CharField('Nombre de la Especialidad', max_length=30, unique=True, blank=False, null=False)
    activo = models.BooleanField('¿Disponible?', default=True)

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ["nombre"]
        verbose_name = 'Especialidad'
        verbose_name_plural = 'Especialidades'


class Empleado(models.Model):
    nombre = models.CharField('Nombres del empleado', max_length = 40)
    apellido = models.CharField('Apellidos del empleado', max_length = 40)
    dui = models.CharField('Número de DUI', max_length = 10, help_text='Formato: XXXXXXXX-X',unique=True)
    nit = models.CharField('Número de NIT', max_length = 17, help_text='Formato: XXXX-XXXXXX-XXX-X',unique=True)
    sexo = models.CharField('Sexo', max_length = 1, choices=SEXO_CHOICES, blank=False, null=False)
    fecha_nacimiento=models.DateField('Fecha de nacimiento', help_text='Formato: DD/MM/AAAA')
    estado_civil = models.ForeignKey(EstadoCivil, null = False)
    telefono = models.CharField('Número de teléfono', max_length = 9, help_text='Formato: XXXX-XXXX',unique=True)
    direccion = models.CharField('Dirección', max_length = 256, help_text='Dirección de su residencia')
    activo = models.BooleanField('¿Activo?', default = True)
    user = models.OneToOneField(User, null = True, help_text='Usuario relacionado')
    especialidad = models.ForeignKey(Especialidad, null=True, blank=True)


    def __str__(self):
        return '{} {}'.format(self.nombre.title(), self.apellido.title())

    class Meta:
        ordering = ['apellido']
        verbose_name = 'Empleado'
        verbose_name_plural = 'Empleados'

class Paciente(models.Model):
    nombre = models.CharField('Nombres del paciente', max_length = 40,blank=False,null=False)
    apellido = models.CharField('Apellidos del paciente', max_length = 40,blank=False,null=False)
    dui = models.CharField('Número de DUI', max_length = 10, blank=True, null=True, help_text='Formato: XXXXXXXX-X',unique=True)
    nit = models.CharField('Número de NIT', max_length = 17, blank=True, null=True, help_text='Formato: XXXX-XXXXXX-XXX-X',unique=True)
    sexo = models.CharField('Sexo', max_length = 1, choices=SEXO_CHOICES,blank=False,null=False)
    fecha_nacimiento = models.DateField('Fecha de nacimiento', help_text='Formato: DD/MM/AAAA',blank=False,null=False)
    telefono = models.CharField('Número de teléfono', max_length = 9, help_text='Formato: XXXX-XXXX',blank=False,null=False, unique=False)
    departamento = models.ForeignKey(Departamento, blank=False, null=False)
    municipio = models.ForeignKey(Municipio, blank=False, null=False)
    direccion = models.CharField('Dirección', max_length = 256, help_text='Dirección de su residencia',blank=False,null=False)
    estado_civil = models.ForeignKey(EstadoCivil, null = False,blank=False)
    email = models.EmailField('Correo Electrónico', max_length=254, blank=True, null=True, unique=True)
    activo = models.BooleanField('¿Activo?', default = True)

    def get_absolute_url(self):
        # return reverse('consultarPacientes')
        return reverse('listar', kwargs={"num_expediente": self.expediente})

    def __str__(self):
        return '{} - {} {}'.format(self.expediente.num_expediente, self.nombre.title(), self.apellido.title())

    class Meta:
        ordering = ['expediente']
        verbose_name = 'Paciente'
        verbose_name_plural = 'Pacientes'


class Expediente(models.Model):
    num_expediente = models.CharField('Número de expediente', max_length = 5,unique=True)
    paciente = models.OneToOneField(Paciente, null = False, help_text='Expediente asignado')
    fecha_apertura = models.DateField('Fecha de apertura', auto_now_add=True)
    diagnostico = models.BooleanField('Diagnóstico', default=False, help_text='¿Presenta diagnóstico?')
    tratamiento = models.BooleanField('Tratamiento', default=False, help_text='¿Ya esta en tratamiento?')
    lugar = models.CharField('Lugar de atención', max_length=80, blank=True, null=True)
    creado_por = models.ForeignKey(Empleado, null = False)


    def __str__(self):
        return self.num_expediente

    class Meta:
        ordering = ["num_expediente"]
        verbose_name = 'Expediente'
        verbose_name_plural = 'Expedientes'


class Responsable(models.Model):
    nombre = models.CharField('Nombres del responsable', max_length = 40)
    apellido = models.CharField('Apellidos del responsable', max_length = 40)
    parentesco = models.CharField('Parentesco', max_length = 10)
    telefono = models.CharField('Número de teléfono', max_length=9, help_text='Formato: XXXX-XXXX', blank=False, null=True, unique=False)
    paciente = models.ForeignKey(Paciente, null = False, blank=False, help_text='Responsable de')


    def __str__(self):
        return '{} {}'.format(self.nombre, self.apellido)

    class Meta:
        ordering = ['apellido']
        verbose_name = 'Responsable'
        verbose_name_plural = 'Responsables'

class Servicio(models.Model):
    nombre = models.CharField('Nombre del Servicio', max_length=30, unique=True, blank=False, null=False)
    tipo = models.CharField('Tipo de Servicio', max_length=1, choices=(('1', 'Básico'),('2', 'Médico')), blank=False, null=False)
    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ["tipo"]
        verbose_name = 'Servicio'
        verbose_name_plural = 'Servicios'

class Tarifa(models.Model):
    nombre = models.CharField('Descripción', max_length=255, blank=False, null=False, unique=True)
    monto=models.DecimalField('Monto', max_digits=5, decimal_places=2, blank=False, null=False, validators = [MinValueValidator(0)])
    tarifa_padre = models.ForeignKey('self', null=True, blank=True)
    examen = models.ForeignKey('control.Examen', null=True, blank=True)

    def __str__(self):
        return self.nombre

    class Meta:
        unique_together = ('tarifa_padre', 'examen',)
        verbose_name = 'Tarifa'
        verbose_name_plural = 'Tarifas'


class Socioeconomico(models.Model):
    vivienda = models.BooleanField('Vivienda', default=False, blank=False, null=False)
    vehiculo = models.BooleanField('Vehiculo', default=False, blank=False, null=False)
    trabaja = models.BooleanField('Trabaja', default=False, blank=False, null=False)
    esp_trabajo = models.CharField('Especifique Trabajo', max_length = 300,  blank=True, null=True)
    estudia = models.BooleanField('Estudia', default=False, blank=False, null=False)
    negocio = models.BooleanField('Negocio', default=False, blank=False, null=False)
    pensionado = models.BooleanField('Pensionado', default=False, blank=False, null=False)
    tarifa = models.ForeignKey(Tarifa, null=False, blank=False, help_text='Tarifa asignada')
    expediente = models.OneToOneField(Expediente, help_text='Numero de Expediente relacionado')
    servicios = models.ManyToManyField(Servicio, blank=True)

    def __str__(self):
        return self.expediente.num_expediente


    class Meta:
        ordering = ["expediente"]
        verbose_name = 'Estudio Socioeconómico'
        verbose_name_plural = 'Estudios Socioeconómicos'

class Participante(models.Model):
    nombre = models.CharField('Nombres del participante', max_length=40, blank=False, null=False)
    apellido = models.CharField('Apellidos del participante', max_length=40, blank=False, null=False)
    telefono = models.CharField('Número de teléfono', max_length=9, help_text='Formato: XXXX-XXXX', blank=False, null=False, unique=False)
    departamento = models.ForeignKey(Departamento, blank=False, null=False)
    municipio = models.ForeignKey(Municipio, blank=False, null=False)
    direccion = models.CharField('Dirección', max_length=256, help_text='Dirección de su residencia', blank=False, null=False)
    email = models.EmailField('Correo Electrónico', max_length=254, blank=True, null=True, unique=True)
    activo = models.BooleanField('¿Activo?', default=True)

    def __str__(self):
        return '{} {}'.format(self.nombre, self.apellido)


    class Meta:
        ordering = ["nombre"]

class Ponente(models.Model):
    nombre = models.CharField('Nombres del Ponente', max_length=40, blank=False, null=False)
    apellido = models.CharField('Apellidos del Ponente', max_length=40, blank=False, null=False)
    telefono = models.CharField('Número de teléfono Fijo', max_length=9, help_text='Formato: XXXX-XXXX', blank=True, null=True)
    telefono_cel = models.CharField('Número de teléfono Celular', max_length=9, help_text='Formato: XXXX-XXXX', blank=False, null=False, unique=False)
    email = models.EmailField('Correo Electrónico', max_length=254, blank=True, null=True, unique=True)
    profesion = models.CharField('Profesion del Ponente', max_length=40, blank=False, null=False)
    activo = models.BooleanField('¿Activo?', default=True)

    def __str__(self):
        return '{} {}'.format(self.nombre, self.apellido)

    class Meta:
        ordering = ["nombre"]

class GrupoApoyo(models.Model):
    tema = models.CharField('Tema a tratar', max_length=128, blank=False, null=False)
    lugar = models.CharField('Lugar', max_length=80, blank=False, null=False)
    fecha = models.DateField('Fecha', help_text = 'Formato: DD/MM/AAAA',blank=False, null=False)
    hora = models.TimeField('Hora', blank=False, null=False)
    estado = models.CharField(max_length=1, choices=GRUPO_CHOICES, blank=False, null=False, default='1')
    pacientes = models.ManyToManyField(Paciente, help_text='Pacientes que asistieron al Grupo de Apoyo', blank=True)
    participantes = models.ManyToManyField(Participante, help_text='Participantes que asistieron al Grupo de Apoyo', blank=True)
    ponente = models.ForeignKey(Ponente, null=False, blank=False, help_text='Ponente asignado')

    def __str__(self):
        return '{}: {}'.format(self.fecha, self.tema)

    def get_estado_display(self):
        for estado in GRUPO_CHOICES:
            if self.estado == estado[0]:
                return estado[1]

    class Meta:
        ordering = ["fecha", "hora"]
        verbose_name = 'Grupo de Apoyo'
        verbose_name_plural = 'Grupos de Apoyo'


