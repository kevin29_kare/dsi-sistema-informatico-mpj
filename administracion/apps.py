from django.apps import AppConfig


class AdministracionConfig(AppConfig):
    name = 'administracion'
    icon = '<i class="material-icons">local_hospital</i>'

