# -*- coding: utf8 -*-
"""
Nombre del Módulo: Archivo de vistas para la app “administracion”
Autores: Francisco Rivas, Kevin Rodríguez, Fernando Rivas, Irving Oliva, Martin Valle
Última fecha de modificación: 04/10/2017
"""
from builtins import memoryview

from django.shortcuts import render, redirect
from django.contrib.auth.models import User, Permission
from django.db.models import Q
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.core.urlresolvers import reverse_lazy
from django import  forms
from django.template import RequestContext
from .models import  Paciente, Empleado, Expediente, Responsable, Servicio, Tarifa, Socioeconomico,Especialidad, GrupoApoyo, Participante, Ponente, Departamento, Municipio
from control.models import PlanTratamiento, Resultado, CitaMedica, Examen, Medicamento, LoteMedicamento
from donaciones.models import Institucion, Donador, BienMueble, LoteBienMueble, Donacion
from .forms import PacienteForm, UsuarioForm, ExpedienteForm, ResponsableForm, EstudioForm
from administracion.models import Paciente,Empleado, EstadoCivil
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse_lazy, reverse
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from material import *
import datetime
from datetime import timedelta
from operator import itemgetter
from operator import itemgetter
from autenticacion.urls import *
import locale

# Necesario para la generación de PDF
from django.views import View
from wkhtmltopdf.views import PDFTemplateResponse

# Create your views here.
# Vista para Index
def index(request):
    return render(request, 'admin/base_site.html', {})

# Inician vistas para Usuarios
@permission_required('auth.add_user')
@login_required
def registroUsuario(request):

    mensaje = ""

    if request.method == 'POST':
        form=UsuarioForm(request.POST or None)
        if form.is_valid():
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password1']
            password2 = form.cleaned_data['password2']
            if password2 == password:
                new_user = User.objects.create_user(username, email, password)
                new_user.is_active = True
                try:
                    new_user.save()
                    mensaje = "Usuario Creado con Exito"
                    form = UsuarioForm()
                except:
                    mensaje = "Error al Crear Usuario"
            else:
                mensaje = "Contraseñas no coinciden"
                return render(request, 'administracion/usuarioform.html', {'form': form, 'mensaje': mensaje, }, )
        else:
            mensaje = "Datos no validos"
    else:
        form=UsuarioForm()

    return render(request, 'administracion/usuarioform.html', {'form':form, 'mensaje': mensaje, }, )
# Finalizan Vistas para Usuarios

@login_required
@permission_required('administracion.delete_expediente') # Consultar Expedientes
def expediente(request, pk):
    mensaje = ""
    expediente = Expediente.objects.get(pk = pk)
    paciente = Paciente.objects.get(pk = expediente.paciente_id).__str__()
    citas = CitaMedica.objects.none()
    resultados = Resultado.objects.none()
    planes = PlanTratamiento.objects.none()
    if request.POST.get('fecha_desde'):
        a = request.POST.get('fecha_desde')
    if request.POST.get('fecha_hasta'):
        b = request.POST.get('fecha_hasta')
    if request.POST.get('select'):
        if request.POST.get('select') == '1':
            planes = PlanTratamiento.objects.filter(expediente_id=pk)
            citas = CitaMedica.objects.filter(estado='3').filter(plan_tratamiento__in = planes)
            if request.POST.get('fecha_desde'):
                citas = citas.filter(fecha__gte = a[6:] + "-" + a[3:5]+ "-" + a[:2])
            if request.POST.get('fecha_hasta'):
                citas = citas.filter(fecha__lte = b[6:] + "-" + b[3:5]+ "-" + b[:2])
            # Vaciando la lista de Planes de Tratamiento
            planes = PlanTratamiento.objects.none()
        elif request.POST.get('select') == '2':
            planes = PlanTratamiento.objects.filter(expediente_id=pk)
            if request.POST.get('fecha_desde'):
                planes = planes.filter(fecha_inicio__gte = a[6:] + "-" + a[3:5]+ "-" + a[:2])
            if request.POST.get('fecha_hasta'):
                planes = planes.filter(fecha_inicio__lte = b[6:] + "-" + b[3:5]+ "-" + b[:2])
        else:
            resultados = Resultado.objects.filter(expediente_id=pk)
            if request.POST.get('fecha_desde'):
                resultados = resultados.filter(fecha_realizacion__gte = a[6:] + "-" + a[3:5]+ "-" + a[:2])
            if request.POST.get('fecha_hasta'):
                resultados = resultados.filter(fecha_realizacion__lte = b[6:] + "-" + b[3:5]+ "-" + b[:2])
    else:
        planes = PlanTratamiento.objects.filter(expediente_id = pk)
        resultados = Resultado.objects.filter(expediente_id = pk)
        citas = CitaMedica.objects.filter(estado='3').filter(plan_tratamiento__in = planes)
        if request.POST.get('fecha_desde'):
            planes = planes.filter(fecha_inicio__gte=a[6:] + "-" + a[3:5]+ "-" + a[:2])
            resultados = resultados.filter(fecha_realizacion__gte = a[6:] + "-" + a[3:5]+ "-" + a[:2])
            citas = citas.filter(fecha__gte=a[6:] + "-" + a[3:5] + "-" + a[:2])
        if request.POST.get('fecha_hasta'):
            planes = planes.filter(fecha_inicio__lte = b[6:] + "-" + b[3:5]+ "-" + b[:2])
            resultados = resultados.filter(fecha_realizacion__lte = b[6:] + "-" + b[3:5]+ "-" + b[:2])
            citas = citas.filter(fecha__lte = b[6:] + "-" + b[3:5]+ "-" + b[:2])
    try:
        socioeconomico = expediente.socioeconomico
    except:
        socioeconomico = None
    object_list = []
    if planes:
        for plan in planes:
            row=[1, plan.fecha_inicio, plan]
            object_list.append(row)
    if resultados:
        for resultado in resultados:
            row=[2, resultado.fecha_realizacion, resultado]
            object_list.append(row)
    if citas:
        for cita in citas:
            row = [3, cita.fecha, cita]
            object_list.append(row)

    object_list = sorted(object_list, key=itemgetter(1), reverse = True)

    return render(request, 'administracion/expediente.html', {'expediente': expediente, 'paciente': paciente,
                                                              'object_list': object_list, 'socioeconomico': socioeconomico,
                                                              'mensaje': mensaje, })

#Función para obtener correlativo del Número del Expediente (No es vista)
def get_correlativo():
    """
    Genera el correlativo del Expediente.

    Parámetros: N/A

    Valor de Retorno: correlativo – Cadena de caracteres con el correlativo generado
    """

    year = datetime.datetime.now().year.__str__()[2:]
    expedientes = Expediente.objects.filter(num_expediente__endswith=year)
    if expedientes:
        last_num_exp = expedientes.last().num_expediente
        correlativo = str(int(last_num_exp[:3]) + 1).zfill(3) + year
    else:
        correlativo="001" + year
    return correlativo

class PreDiagnostico(UpdateView):
    model = Expediente
    fields = ['num_expediente', 'creado_por', 'diagnostico', 'tratamiento', 'lugar']
    template_name = 'administracion/pre_diagnostico.html'

    layout = Layout(Fieldset('Expediente:'), Row('num_expediente', 'creado_por',), Row('diagnostico'), Row('tratamiento'), Row('lugar'))

    def get_form(self):
        form = super(PreDiagnostico, self).get_form()
        form.fields['num_expediente'].widget.attrs['readonly'] = True
        form.fields['creado_por'].widget.attrs['readonly'] = True
        form.fields['diagnostico'].widget.attrs['readonly'] = True
        form.fields['tratamiento'].widget.attrs['readonly'] = True
        form.fields['lugar'].widget.attrs['readonly'] = True
        return form
# Finalizan Vistas para Expediente

# Inician vistas para Pacientes
@login_required
@permission_required(('administracion.add_paciente', 'administracion.add_expediente', 'administracion.add_responsable'))
def registroPaciente(request):
    mensaje = ""
    empleado = None
    try:
        empleado = Empleado.objects.get(user = request.user.id)
    except:
        mensaje = "El usuario actual, no tiene asignado un Empleado! Asignar un Empleado antes de crear un paciente."
    #Cuando es GET
    if request.method == 'GET' or not empleado:
        paciente_form = PacienteForm(prefix='paciente')
        responsable_form = ResponsableForm(prefix='responsable')
        expediente_form = ExpedienteForm(prefix='expediente', initial={"num_expediente": get_correlativo()})

    #Cuando es POST
    if request.method == 'POST' and empleado:
        paciente_form = PacienteForm(request.POST or None, prefix = 'paciente')
        responsable_form = ResponsableForm(request.POST or None, prefix = 'responsable')
        expediente_form = ExpedienteForm(request.POST or None, prefix = 'expediente')

        if expediente_form.is_valid() and responsable_form.is_valid() and paciente_form.is_valid():

            nombreP = paciente_form.cleaned_data['nombre']
            apellidoP = paciente_form.cleaned_data['apellido']
            duiP = paciente_form.cleaned_data['dui']
            nitP = paciente_form.cleaned_data['nit']
            sexoP = paciente_form.cleaned_data['sexo']
            fecha_nacimientoP = paciente_form.cleaned_data['fecha_nacimiento']
            telefonoP = paciente_form.cleaned_data['telefono']
            departamentoP = paciente_form.cleaned_data['departamento']
            municipioP = paciente_form.cleaned_data['municipio']
            direccionP = paciente_form.cleaned_data['direccion']
            estado_civilP = paciente_form.cleaned_data['estado_civil']
            correo = paciente_form.cleaned_data['email']

            nombreR = responsable_form.cleaned_data['nombre']
            apellidoR = responsable_form.cleaned_data['apellido']
            parentestoR = responsable_form.cleaned_data['parentesco']
            telefonoR = responsable_form.cleaned_data['telefono']

            nExp = expediente_form.cleaned_data['num_expediente']
            nDiagnostico = expediente_form.cleaned_data['diagnostico']
            nTratamiento = expediente_form.cleaned_data['tratamiento']
            nLugar = expediente_form.cleaned_data['lugar']
            nCreado_por = Empleado.objects.get(user_id=request.user.id)

            nPaciente = Paciente.objects.create(
                nombre=nombreP,
                apellido=apellidoP,
                dui=duiP,
                nit=nitP,
                sexo=sexoP,
                fecha_nacimiento = fecha_nacimientoP,
                telefono=telefonoP,
                departamento=departamentoP,
                municipio=municipioP,
                direccion = direccionP,
                estado_civil = estado_civilP,
                email = correo
            )

            nResponsable = Responsable.objects.create(
                nombre = nombreR,
                apellido=apellidoR,
                parentesco=parentestoR,
                paciente = nPaciente,
                telefono = telefonoR
            )

            nExpediente = Expediente.objects.create(
                num_expediente = nExp,
                paciente = nPaciente,
                diagnostico = nDiagnostico,
                tratamiento = nTratamiento,
                lugar = nLugar,
                creado_por = nCreado_por
            )
            # Creando un Plan de Tratamiento inicial para el paciente
            PlanTratamiento.objects.create(expediente = nPaciente.expediente, estado = '1', fecha_inicio = datetime.date.today())
            mensaje = "Paciente Creado con Exito"
            #Limpiando campos después de guardar (Reset Forms)
            paciente_form = PacienteForm(prefix='paciente')
            responsable_form = ResponsableForm(prefix='responsable')
            expediente_form = ExpedienteForm(prefix='expediente', initial={"num_expediente": get_correlativo()})

    extra_context = {
        'municipios': Municipio.objects.all(),
        'paciente_form' : paciente_form,
        'responsable_form' : responsable_form,
        'expediente_form' : expediente_form,
        'mensaje': mensaje
    }

    return render(request, 'administracion/regform.html', extra_context)

@login_required
def consultarPacientes(request):
    if request.user.has_perm('administracion.change_paciente') or request.user.has_perm('administracion.delete_paciente'):
        pacientes_list = Paciente.objects.all()
        return render(request, 'administracion/consultaPacientes.html', {'pacientes_list': pacientes_list, })
    else:
        return HttpResponseRedirect('/login/?next=/pacientes/')

@login_required
@permission_required('administracion.delete_paciente')
def confirmar_desactivar_paciente(request, paciente_id):
    paciente=Paciente.objects.get(pk=paciente_id)
    mensaje=""
    if paciente.activo:
        mensaje="desactivar"
    else:
        mensaje="activar"

    return render(request, 'administracion/modificar_estado.html', {'paciente': paciente, 'mensaje': mensaje, })

@login_required
@permission_required('administracion.delete_paciente')
def desactivar_paciente(request, paciente_id):
    paciente = Paciente.objects.get(pk=paciente_id)
    if paciente.activo:
        paciente.activo = 0
    else:
        paciente.activo = 1
    paciente.save()
    return HttpResponseRedirect('/pacientes')

class PacienteUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = Paciente
    fields = ['nombre', 'apellido','dui','nit','fecha_nacimiento','sexo', 'telefono', 'departamento', 'municipio', 'direccion', 'estado_civil','email']
    template_name_suffix = '_update_form'
    success_message = "Paciente modificado con éxito"
    permission_required = 'administracion.change_paciente'

    layout = Layout(Fieldset('Modificar Paciente:'), Row('nombre', 'apellido'), Row('dui', 'nit'),
                    Row('fecha_nacimiento', 'telefono'), Row('sexo', 'estado_civil'), Row('email','departamento'), Row('municipio','direccion'))

    def get_form(self):
        form = super(PacienteUpdate, self).get_form()
        form.fields['estado_civil'].widget = forms.RadioSelect(
            choices=((x.id, x.nombre) for x in EstadoCivil.objects.all()))
        form.fields['sexo'].widget = forms.RadioSelect(choices=(
            ('M', 'Masculino'),
            ('F', 'Femenino'),
        ))
        return form
    def get_context_data(self, **kwargs):
        context = super(PacienteUpdate, self).get_context_data(**kwargs)
        context['municipios'] = Municipio.objects.all()
        return context

    def get_success_url(self):
        return reverse("consultarPacientes")

@login_required
@permission_required('administracion.change_responsable')
def actualizar_responsable(request, paciente_id):
    mensaje = " "
    responsable = Responsable.objects.get(paciente_id=paciente_id)
    form = ResponsableForm(data = request.POST or None, instance=responsable)
    if form.is_valid():
        form.save()
        mensaje = "Responsable Actualizado con Éxito"
        return HttpResponseRedirect('/pacientes')
    return render(request, 'administracion/modificar_responsable.html', {'form': form, 'mensaje': mensaje})
# Finalizan Vistas para Pacientes

# Inician Vistas para Empleados
class registroEmpledo(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = Empleado
    fields = ['nombre', 'apellido','dui', 'nit', 'sexo', 'fecha_nacimiento', 'estado_civil' ,'telefono', 'direccion', 'especialidad', 'user']
    template_name = 'administracion/empleadoform.html'
    success_url = reverse_lazy('registrar_empleado')
    success_message = "Empleado creado con éxito."
    permission_required = 'administracion.add_empleado'
    layout = Layout(Fieldset('Registrar Empleado:'),Row('nombre', 'apellido'), Row('dui', 'nit'),Row('fecha_nacimiento', 'telefono'), Row('sexo', 'estado_civil'), 'direccion', 'especialidad', 'user')

    def get_form(self):
        form = super(registroEmpledo, self).get_form()
        form.fields['estado_civil'].widget =forms.RadioSelect(choices=( (x.id, x.nombre) for x in EstadoCivil.objects.all() ))
        form.fields['sexo'].widget = forms.RadioSelect(choices=(
            ('M', 'Masculino'),
            ('F', 'Femenino'),
        ))
        return form

    def form_valid(self, form):
        self.object = form.save(commit=False)
        # Si es medico se le asignan los permisos basicos para un medico
        if self.object.especialidad:
            perms = Permission.objects.filter(Q(codename__contains='dosis') | Q(codename__contains='receta') | Q(codename__contains='plantratamiento') | Q(codename__contains='resultado') | Q(codename='delete_expediente') | Q(codename='change_citamedica') | Q(codename='delete_citamedica'))
            user = User.objects.get(pk = self.object.user.id)
            user.user_permissions.set(perms)
            user.save()
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

@login_required
def consultarEmpleados(request):
    if request.user.has_perm('administracion.change_empleado') or request.user.has_perm('administracion.delete_empleado'):
        empleados_list = Empleado.objects.all()
        return render(request, 'administracion/consultarEmpleados.html', {'empleados_list': empleados_list, })
    else:
        return HttpResponseRedirect('/login/?next=/empleados/')

@permission_required('administracion.delete_empleado')
@login_required
def confirmar_desactivar_empleado(request, empleado_id):
    empleado=Empleado.objects.get(pk=empleado_id)
    mensaje=""
    if empleado.activo:
        mensaje="desactivar"
    else:
        mensaje="activar"

    return render(request, 'administracion/modificar_empleado.html', {'empleado': empleado, 'mensaje': mensaje, })

@login_required
@permission_required('administracion.delete_empleado')
def desactivar_empleado(request, empleado_id):
    empleado = Empleado.objects.get(pk=empleado_id)
    if empleado.activo:
        empleado.activo = 0
    else:
        empleado.activo = 1
    empleado.save()
    return HttpResponseRedirect('/empleados')

class EmpleadoUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = Empleado
    fields = ['nombre', 'apellido', 'dui', 'nit','fecha_nacimiento', 'sexo','estado_civil','telefono', 'direccion']
    template_name_suffix = '_update_form'
    success_message = "Empleado modificado con éxito"
    permission_required = 'administracion.change_empleado'

    layout = Layout(Fieldset('Modificar Empleado:'), Row('nombre', 'apellido'), Row('dui', 'nit'),
                    Row('fecha_nacimiento', 'telefono'), Row('sexo', 'estado_civil'), 'direccion')

    def get_form(self):
        form = super(EmpleadoUpdate, self).get_form()
        form.fields['estado_civil'].widget = forms.RadioSelect(
            choices=((x.id, x.nombre) for x in EstadoCivil.objects.all()))
        form.fields['sexo'].widget = forms.RadioSelect(choices=(
            ('M', 'Masculino'),
            ('F', 'Femenino'),
        ))
        return form

    def get_success_url(self):
        return reverse("consultarEmpleados")
# Finalizan vistas de empleados

# Inician vistas para Servicios
class AgregarServicio(SuccessMessageMixin, PermissionRequiredMixin,CreateView):
    model = Servicio
    fields = ['nombre', 'tipo']
    template_name = 'administracion/agregar_servicio.html'
    success_message = "Servicio Agregado con Éxito"
    permission_required = 'administracion.add_servicio'
    success_url = reverse_lazy('agregar_servicio')

@login_required
def consultarServicio(request):
    if request.user.has_perm('administracion.change_servicio') or request.user.has_perm('administracion.delete_servicio'):
        servicios_list = Servicio.objects.all()
        return render(request, 'administracion/consultar_servicio.html', {'servicios_list': servicios_list, })
    else:
        return HttpResponseRedirect('/base/')

class ModificarServicio(SuccessMessageMixin, PermissionRequiredMixin,UpdateView):
    model = Servicio
    fields = ['nombre', 'tipo']
    template_name = 'administracion/agregar_servicio.html'
    success_message = "Servicio Modificado con Éxito"
    permission_required = 'administracion.change_servicio'

    def get_context_data(self, **kwargs):
        context = super(ModificarServicio, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("consultarServicio")
# Finalizan vistas para Servicios

# Inician vistas para Tarifas
class AgregarTarifa1(SuccessMessageMixin, PermissionRequiredMixin,CreateView):
    model = Tarifa
    fields = ['nombre', 'monto']
    template_name = 'administracion/agregar_tarifa1.html'
    success_message = "Tarifa Agregada con Éxito"
    permission_required = 'administracion.add_tarifa'
    success_url = reverse_lazy('agregar_tarifa1')

class AgregarTarifa2(SuccessMessageMixin, PermissionRequiredMixin,CreateView):
    model = Tarifa
    fields = ['tarifa_padre', 'examen', 'nombre', 'monto']
    template_name = 'administracion/agregar_tarifa2.html'
    success_message = "Tarifa Agregada con Éxito"
    permission_required = 'administracion.add_tarifa'
    success_url = reverse_lazy('agregar_tarifa2')

    def get_form(self):
        form = super(AgregarTarifa2, self).get_form()
        form.fields['tarifa_padre'].widget = forms.Select(choices=(((x.id, x) for x in Tarifa.objects.filter(tarifa_padre_id__isnull=True))))
        form.fields['nombre'].widget.attrs['readonly'] = True
        return form

@login_required
def consultarTarifa1(request):
    if request.user.has_perm('administracion.change_tarifa') or request.user.has_perm('administracion.delete_tarifa'):
        tarifas1_list = Tarifa.objects.filter(tarifa_padre_id__isnull=True)
        return render(request, 'administracion/consultar_tarifa1.html', {'tarifas1_list': tarifas1_list, })
    else:
        return HttpResponseRedirect('/base/')

class ModificarTarifa1(SuccessMessageMixin, PermissionRequiredMixin,UpdateView):
    model = Tarifa
    fields = ['nombre', 'monto']
    template_name = 'administracion/agregar_tarifa1.html'
    success_message = "Tarifa Modificada con Éxito"
    permission_required = 'administracion.change_tarifa'

    def get_context_data(self, **kwargs):
        context = super(ModificarTarifa1, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("consultarTarifa1")

@login_required
def consultarTarifa2(request):
    if request.user.has_perm('administracion.change_tarifa') or request.user.has_perm('administracion.delete_tarifa'):
        tarifas2_list = Tarifa.objects.filter(tarifa_padre_id__isnull=False)
        return render(request, 'administracion/consultar_tarifa2.html', {'tarifas2_list': tarifas2_list, })
    else:
        return HttpResponseRedirect('/base/')

class ModificarTarifa2(SuccessMessageMixin, PermissionRequiredMixin,UpdateView):
    model = Tarifa
    fields = ['tarifa_padre', 'examen', 'nombre', 'monto']
    template_name = 'administracion/agregar_tarifa2.html'
    success_message = "Tarifa Modificada con Éxito"
    permission_required = 'administracion.change_tarifa'

    def get_context_data(self, **kwargs):
        context = super(ModificarTarifa2, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_form(self):
        form = super(ModificarTarifa2, self).get_form()
        form.fields['tarifa_padre'].widget = forms.Select(
            choices=(((x.id, x) for x in Tarifa.objects.filter(tarifa_padre_id__isnull=True))))
        return form

    def get_success_url(self):
        return reverse("consultarTarifa2")
# Finalizan vistas para Tarifas

# Inician Vistas para Estudio Socio-Economico
@login_required
@permission_required('administracion.add_socioeconomico')
def registroEstudio(request, id):
    mensaje = ""
    #Cuando es GET
    if request.method == 'GET':
        estudio_form = EstudioForm(prefix='estudio')

    #Cuando es POST
    if request.method == 'POST':
        estudio_form = EstudioForm(request.POST or None, prefix = 'estudio')

        if estudio_form.is_valid():

            exp = Expediente.objects.get(pk=id)

            tVivienda = estudio_form.cleaned_data['vivienda']
            tVehiculo = estudio_form.cleaned_data['vehiculo']
            tTrabajo = estudio_form.cleaned_data['trabaja']
            espTrabajo = estudio_form.cleaned_data['esp_trabajo']
            tEstudia = estudio_form.cleaned_data['estudia']
            tNegocio= estudio_form.cleaned_data['negocio']
            ePensionado = estudio_form.cleaned_data['pensionado']
            lServicios = estudio_form.cleaned_data['servicios']
            tarifaSel = estudio_form.cleaned_data['tarifa']

            estudio = Socioeconomico.objects.create(
                expediente=exp,
                vivienda=tVivienda,
                vehiculo=tVehiculo,
                trabaja=tTrabajo,
                esp_trabajo=espTrabajo,
                estudia=tEstudia,
                negocio = tNegocio,
                pensionado=ePensionado,
                tarifa = tarifaSel,
            )
            for servi in lServicios:
                estudio.servicios.add(servi)

            mensaje = "Estudio Creado con Éxito"
            return redirect(reverse_lazy('expediente', kwargs={'pk': estudio.expediente_id }))

    extra_context = {
        'estudio_form' : estudio_form,
        'mensaje': mensaje
    }

    return render(request, 'administracion/agregar_estudio.html', extra_context)


class ModificarEstudio(SuccessMessageMixin, PermissionRequiredMixin,UpdateView):
    model = Socioeconomico
    fields = ['vivienda', 'vehiculo', 'trabaja', 'esp_trabajo', 'estudia', 'negocio', 'pensionado', 'servicios', 'tarifa']
    template_name = 'administracion/modificar_estudio.html'
    success_message = "Estudio Modificado con Éxito"
    permission_required = 'administracion.change_socioeconomico'

    def get_form(self):
        form = super(ModificarEstudio, self).get_form()
        form.fields['servicios'].widget = forms.CheckboxSelectMultiple(
            choices=(((x.id, x.nombre.title()) for x in Servicio.objects.all())))
        form.fields['tarifa'].widget = forms.Select(
            choices=(((x.id, x) for x in Tarifa.objects.filter(tarifa_padre_id__isnull=True))))
        return form

    def get_success_url(self, **kwargs):
        socio = Socioeconomico.objects.get(id=self.kwargs['pk'])
        return reverse_lazy('expediente', kwargs={'pk': socio.expediente_id})
# Finalizan Vistas para Estudio Socio-Economico

# Inician Vistas para Especialidades
class AgregarEspecialidad(SuccessMessageMixin, PermissionRequiredMixin,CreateView):
    model = Especialidad
    fields = ['nombre']
    template_name = 'administracion/agregar_especialidad.html'
    success_message = "Especialidad agregada con Éxito"
    permission_required = 'administracion.add_especialidad'
    success_url = reverse_lazy('agregar_especialidad')

@login_required
def consultarEspecialidad(request):
    if request.user.has_perm('administracion.change_especialidad') or request.user.has_perm('administracion.delete_especialidad'):
        especialidad_list = Especialidad.objects.all()
        return render(request, 'administracion/consultar_especialidad.html', {'especialidad_list': especialidad_list, })
    else:
        return HttpResponseRedirect('/base/')

class ModificarEspecialidad(SuccessMessageMixin, PermissionRequiredMixin,UpdateView):
    model = Especialidad
    fields = ['nombre']
    template_name = 'administracion/agregar_especialidad.html'
    success_message = "Éspecialidad modificada con Éxito "
    permission_required = 'administracion.change_especialidad'

    def get_context_data(self, **kwargs):
        context = super(ModificarEspecialidad, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("consultarEspecialidad")

@permission_required('administracion.delete_especialidad')
@login_required
def confirmar_desactivar_especialidad(request, especialidad_id):
    especialidad = Especialidad.objects.get(pk=especialidad_id)
    mensaje = ""
    if especialidad.activo:
        mensaje = "desactivar"
    else:
        mensaje = "activar"

    return render(request, 'administracion/modificar_especialidad.html', {'especialidad': especialidad, 'mensaje': mensaje, })

@login_required
@permission_required('administracion.delete_especialidad')
def desactivar_especialidad(request, especialidad_id):
    especialidad = Especialidad.objects.get(pk=especialidad_id)
    if especialidad.activo:
        especialidad.activo = 0
    else:
        especialidad.activo = 1
    especialidad.save()
    return HttpResponseRedirect('/especialidad')


# Finalizan Vistas para Especialidades

# Inician Vistas para Grupos de Apoyo

class AgregarGrupo(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = GrupoApoyo
    fields = ['fecha','hora', 'lugar', 'tema', 'ponente']
    template_name = 'administracion/agregar_grupo.html'
    success_message = "Grupo de Apoyo Agregado con Éxito"
    permission_required = 'administracion.add_grupoapoyo'
    success_url = reverse_lazy('agregar_grupo')
    layout = Layout(Row('fecha', 'hora'), Row('ponente', 'tema'), Row('lugar'))

class ModificarGrupo(SuccessMessageMixin, PermissionRequiredMixin,UpdateView):
    model = GrupoApoyo
    fields = ['fecha','hora','lugar','tema', 'estado', 'ponente']
    template_name = 'administracion/agregar_grupo.html'
    success_message = "Grupo de Apoyo Modificado con Éxito "
    permission_required = 'administracion.change_grupoapoyo'
    layout = Layout(Row('fecha', 'hora'), Row('ponente','tema'), Row('lugar', 'estado'))

    def get_context_data(self, **kwargs):
        context = super(ModificarGrupo, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("consultarGrupos")

@login_required
def consultarGrupo(request):
    if request.user.has_perm('administracion.change_grupoapoyo') or request.user.has_perm(
            'administracion.delete_grupoapoyo'):
        grupos_list = GrupoApoyo.objects.all()
        return render(request, 'administracion/consultar_grupos.html', {'grupo_list': grupos_list, })
    else:
        return HttpResponseRedirect('/base/')

class AgregarParticipante(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = Participante
    fields = ['nombre', 'apellido', 'departamento', 'municipio', 'direccion', 'telefono', 'email']
    template_name = 'administracion/agregar_participante.html'
    success_message = "Participante Externo Agregado con Éxito"
    permission_required = 'administracion.add_participante'
    success_url = reverse_lazy('agregar_participante')
    layout = Layout(Row('nombre', 'apellido'), Row('telefono', 'email'), Row('departamento', 'municipio', 'direccion'))

    def get_context_data(self, **kwargs):
        context = super(AgregarParticipante, self).get_context_data(**kwargs)
        context['municipios'] = Municipio.objects.all()
        return context


class ModificarParticipante(SuccessMessageMixin, PermissionRequiredMixin,UpdateView):
    model = Participante
    fields = ['nombre','apellido', 'departamento', 'municipio', 'direccion','telefono', 'email']
    template_name = 'administracion/agregar_participante.html'
    success_message = "Participante Externo Modificado con Éxito "
    permission_required = 'administracion.change_participante'
    layout = Layout(Row('nombre', 'apellido'), Row('telefono', 'email'), Row('departamento', 'municipio', 'direccion'))

    def get_context_data(self, **kwargs):
        context = super(ModificarParticipante, self).get_context_data(**kwargs)
        context['actualizar'] = True
        context['municipios'] = Municipio.objects.all()
        return context

    def get_success_url(self):
        return reverse("consultarParticipantes")

@login_required
def consultarParticipantes(request):
    if request.user.has_perm('administracion.change_participante') or request.user.has_perm(
            'administracion.delete_participante'):
        participantes_list = Participante.objects.all()
        return render(request, 'administracion/consultar_participantes.html', {'participantes_list': participantes_list, })
    else:
        return HttpResponseRedirect('/base/')

@permission_required('administracion.delete_participante')
@login_required
def confirmar_desactivar_participante(request, participante_id):
    participante = Participante.objects.get(pk=participante_id)
    mensaje = ""
    if participante.activo:
        mensaje = "desactivar"
    else:
        mensaje = "activar"

    return render(request, 'administracion/modificar_participante.html', {'participante': participante, 'mensaje': mensaje, })

@login_required
@permission_required('administracion.delete_participante')
def desactivar_participante(request, participante_id):
    participante = Participante.objects.get(pk=participante_id)
    if participante.activo:
        participante.activo = 0
    else:
        participante.activo = 1
    participante.save()
    return HttpResponseRedirect('/participantes')


# Finalizan Vistas para Grupos de Apoyo

@login_required
@permission_required('administracion.change_grupoapoyo')
def asistencia_participantes(request, grupo_apoyo_id):
    mensaje=""
    grupo_apoyo = GrupoApoyo.objects.get(pk=grupo_apoyo_id)
    if request.POST.getlist('pre-selected-options'):
        lista=request.POST.getlist('pre-selected-options')
        i = 0
        for item in lista:
            lista[i] = lista[i].split(",")
            i+=1
        participantes = []
        pacientes = []
        i = 0
        for item in lista:
            if item[0] == '0':
                participantes.append(int(item[1]))
            else:
                pacientes.append(int(item[1]))
        try:
            grupo_apoyo.participantes.set(participantes)
            grupo_apoyo.pacientes.set(pacientes)
            mensaje = "Asistencia actualizada con éxito"
        except:
            mensaje = "Error al Actualizar"

    participantes = Participante.objects.all()  #Sí son instancias
    pacientes = Paciente.objects.all()  #Sí son instancias

    grupo_apoyo_participantes_list = list(grupo_apoyo.participantes.all()) #No son instancias
    grupo_apoyo_pacientes_list = list(grupo_apoyo.pacientes.all()) #No son instancias

    participantes_noasistieron_list = []
    participantes_asistieron_list = []
    for participante in participantes:
        b = False  # No asistió
        for grupo_apoyo_participante in grupo_apoyo_participantes_list:
            if participante.id is grupo_apoyo_participante.id:
                b=True #Sí asistió
        if b: #¿Asistió?
            participantes_asistieron_list.append(participante)
        else:
            participantes_noasistieron_list.append(participante)

    pacientes_noasistieron_list = []
    pacientes_asistieron_list = []
    for paciente in pacientes:
        b = False  # No asistió
        for grupo_apoyo_paciente in grupo_apoyo_pacientes_list:
            if paciente.id is grupo_apoyo_paciente.id:
                b = True  # Sí asistió
        if b:  # ¿Asistió?
            pacientes_asistieron_list.append(paciente)
        else:
            pacientes_noasistieron_list.append(paciente)

    return render(request, 'administracion/asistencia_participantes.html', {'participantes_asistieron_list': participantes_asistieron_list,
                                                                            'participantes_noasistieron_list': participantes_noasistieron_list,
                                                                            'pacientes_asistieron_list': pacientes_asistieron_list,
                                                                            'pacientes_noasistieron_list': pacientes_noasistieron_list, 'mensaje': mensaje})


class AgregarPonente(SuccessMessageMixin, PermissionRequiredMixin, CreateView):
    model = Ponente
    fields = ['nombre', 'apellido','telefono','telefono_cel','email', 'profesion']
    template_name = 'administracion/agregar_ponente.html'
    success_message = "Ponente Agregado con Éxito"
    permission_required = 'administracion.add_ponente'
    success_url = reverse_lazy('agregar_ponente')
    layout = Layout(Row('nombre', 'apellido'), Row('telefono', 'telefono_cel'), Row('email', 'profesion'))

@login_required
def consultarPonentes(request):
    if request.user.has_perm('administracion.change_ponente') or request.user.has_perm(
            'administracion.delete_ponente'):
        ponentes_list = Ponente.objects.all()
        return render(request, 'administracion/consultar_ponentes.html', {'ponentes_list': ponentes_list, })
    else:
        return HttpResponseRedirect('/base/')

class ModificarPonente(SuccessMessageMixin, PermissionRequiredMixin,UpdateView):
    model = Ponente
    fields = ['nombre', 'apellido','telefono','telefono_cel','email', 'profesion']
    template_name = 'administracion/agregar_ponente.html'
    success_message = "Ponente Modificado con Éxito "
    permission_required = 'administracion.change_ponente'
    layout = Layout(Row('nombre', 'apellido'), Row('telefono', 'telefono_cel'), Row('email', 'profesion'))

    def get_context_data(self, **kwargs):
        context = super(ModificarPonente, self).get_context_data(**kwargs)
        context['actualizar'] = True
        return context

    def get_success_url(self):
        return reverse("consultarPonentes")

@permission_required('administracion.delete_ponente')
@login_required
def confirmar_desactivar_ponente(request, ponente_id):
    ponente = Ponente.objects.get(pk=ponente_id)
    mensaje = ""
    if ponente.activo:
        mensaje = "desactivar"
    else:
        mensaje = "activar"

    return render(request, 'administracion/modificar_ponente.html', {'ponente': ponente, 'mensaje': mensaje, })

@login_required
@permission_required('administracion.delete_ponente')
def desactivar_ponente(request, ponente_id):
    ponente = Ponente.objects.get(pk=ponente_id)
    if ponente.activo:
        ponente.activo = 0
    else:
        ponente.activo = 1
    ponente.save()
    return HttpResponseRedirect('/ponentes')


@login_required
@permission_required('control.change_dosis')
def reportes(request):
    return render(request, 'reportes/lista_reportes.html', {})

@login_required
@permission_required('control.change_dosis')
def consultas_medicos(request):
    locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
    if request.method == 'POST':
        presentacion = request.POST.get('select')
        g = False
        if request.POST.get('fecha_desde'):

            desde = datetime.datetime.strptime(request.POST.get('fecha_desde'), '%d/%m/%Y').strftime("%d de %B de %Y")
        else:
            desde = "el origen de los tiempos"
        fecha_hasta = request.POST.get('fecha_hasta')
    else:
        presentacion = '1'  # En un GET la presentación por defecto será Individual
        g = True
        fecha_desde = (datetime.date.today() - timedelta(datetime.date.today().day-1)).strftime("%Y-%m-%d")
        desde = (datetime.date.today() - timedelta(datetime.date.today().day-1)).strftime("%d de %B de %Y")
    try:
        hasta = datetime.datetime.strptime(fecha_hasta, '%d/%m/%Y').strftime("%d de %B de %Y")
    except:
        hasta = datetime.date.today().strftime("%d de %B de %Y")
    medicos_list = []
    medicos = Empleado.objects.filter(activo = 1).filter(especialidad_id__isnull=False)
    for medico in medicos:
        citas = CitaMedica.objects.filter(medico = medico).filter(estado = '3')
        if g:
            citas = citas.filter(fecha__gte = fecha_desde)
        if request.POST.get('fecha_desde'):
            a = request.POST.get('fecha_desde')
            citas = citas.filter(fecha__gte = a[6:] + "-" + a[3:5]+ "-" + a[:2])
        if request.POST.get('fecha_hasta'):
            b = request.POST.get('fecha_hasta')
            citas = citas.filter(fecha__lte = b[6:] + "-" + b[3:5]+ "-" + b[:2])
        medicos_list.append([medico, citas])
    return render(request, 'reportes/consultas_medicos.html', {'medicos_list': medicos_list, 'desde': desde, 'hasta': hasta, 'presentacion': presentacion})

@login_required
@permission_required('control.change_dosis')
def consultas_especialidades(request):
    locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
    if request.method == 'POST':
        g = False
        if request.POST.get('fecha_desde'):
            desde = datetime.datetime.strptime(request.POST.get('fecha_desde'), '%d/%m/%Y').strftime("%d de %B de %Y")
        else:
            desde = "el origen de los tiempos"
        fecha_hasta = request.POST.get('fecha_hasta')
    else:
        g = True
        fecha_desde = (datetime.date.today() - timedelta(datetime.date.today().day-1)).strftime("%Y-%m-%d")
        desde = (datetime.date.today() - timedelta(datetime.date.today().day-1)).strftime("%d de %B de %Y")
    try:
        hasta = datetime.datetime.strptime(fecha_hasta, '%d/%m/%Y').strftime("%d de %B de %Y")
    except:
        hasta = datetime.date.today().strftime("%d de %B de %Y")

    especialidad = Especialidad.objects.all()
    medicos = Empleado.objects.exclude(especialidad__isnull=True, especialidad__in=especialidad)

    # Filtrando las Citas Médicas
    citas = CitaMedica.objects.filter(estado = '3')
    if g:
        citas = citas.filter(fecha__gte = fecha_desde)
    if request.POST.get('fecha_desde'):
        a = request.POST.get('fecha_desde')
        citas = citas.filter(fecha__gte = a[6:] + "-" + a[3:5]+ "-" + a[:2])
    if request.POST.get('fecha_hasta'):
        b = request.POST.get('fecha_hasta')
        citas = citas.filter(fecha__lte = b[6:] + "-" + b[3:5]+ "-" + b[:2])

    # Contando las Consultas Gratuitas.
    citas_gratis = 0
    for cita in citas:
        if cita.precio_consulta == 0 or cita.precio_consulta == None:
            citas_gratis += 1
    # Contando Consultas por Especialidad
    esp_list = []
    for esp in especialidad:
        medicos = Empleado.objects.filter(especialidad=esp)
        esp_citas = 0
        for medico in medicos:
            esp_citas += citas.filter(medico=medico).count()
        esp_list.append([esp, esp_citas])
    context = {
        'citas_gratis': citas_gratis,
        'esp_list': esp_list,
        'desde': desde,
        'hasta': hasta
    }

    return render(request, 'reportes/consultas_especialidades.html', context)

@login_required
@permission_required('control.change_dosis')
def examenes_realizados(request):
    locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
    if request.method == 'POST':
        g = False
        if request.POST.get('fecha_desde'):
            desde = datetime.datetime.strptime(request.POST.get('fecha_desde'), '%d/%m/%Y').strftime("%d de %B de %Y")
        else:
            desde = "el origen de los tiempos"
        fecha_hasta = request.POST.get('fecha_hasta')
    else:
        g = True
        fecha_desde = (datetime.date.today() - timedelta(datetime.date.today().day-1)).strftime("%Y-%m-%d")
        desde = (datetime.date.today() - timedelta(datetime.date.today().day-1)).strftime("%d de %B de %Y")
    try:
        hasta = datetime.datetime.strptime(fecha_hasta, '%d/%m/%Y').strftime("%d de %B de %Y")
    except:
        hasta = datetime.date.today().strftime("%d de %B de %Y")

    # Filtrando las Citas Médicas
    citas = CitaMedica.objects.exclude(examen_id__isnull=True).filter(estado = '3')
    if g:
        citas = citas.filter(fecha__gte = fecha_desde)
    if request.POST.get('fecha_desde'):
        a = request.POST.get('fecha_desde')
        citas = citas.filter(fecha__gte = a[6:] + "-" + a[3:5]+ "-" + a[:2])
    if request.POST.get('fecha_hasta'):
        b = request.POST.get('fecha_hasta')
        citas = citas.filter(fecha__lte = b[6:] + "-" + b[3:5]+ "-" + b[:2])

    # Calculando Examenes Realizados.
    citas_list = []
    for c in citas:
        try:
            precio = c.examen.tarifa_set.get(
                tarifa_padre=Socioeconomico.objects.get(expediente=c.plan_tratamiento.expediente).tarifa).monto
        except:
            precio = None
        citas_list.append([c, precio])
    context = {
        'citas_list': citas_list,
        'desde': desde,
        'hasta': hasta
    }

    return render(request, 'reportes/examenes_realizados.html', context)


class ReporteMedicamentosPDFView(View):
    template_name = 'reportes/reporte_medicamentos.html'

    def get(self, request, *args, **kwargs):
        medicamentos = Medicamento.objects.filter(estado = '1')
        fecha = datetime.date.today()
        lista_med = []
        for med in medicamentos:
            lotes = LoteMedicamento.objects.filter(medicamento = med, existencias__gt = 0)
            # Calcular medicamentos vencidos
            cant = 0
            for lote in lotes:
                if lote.fecha_vencimiento < fecha:
                    cant += lote.existencias
            lista_med.append([med, lotes, cant])

        context = {
            'lista_med': lista_med,
            'fecha': fecha.strftime("%d/%m/%Y")
        }

        response = PDFTemplateResponse(request=request, template=self.template_name, filename="reporte_medicamentos_" + fecha.strftime("%d_%m_%Y") + ".pdf",
                                       context=context,
                                       show_content_in_browser=True, cmd_options={
                'margin-top': 10,
                'zoom': 1,
                'viewport-size': '1366 x 513',
                'javascript-delay': 1000,
                'footer-center': '[page]/[topage]',
                'no-stop-slow-scripts': True
            },
                                       )
        return response

@login_required()
@permission_required('control.change_dosis')
def donaciones(request):
    locale.setlocale(locale.LC_TIME, 'es_ES.UTF-8')
    if request.method == 'POST':
        g = False
        presentacion = request.POST.get('select')
        if request.POST.get('fecha_desde'):
            desde = datetime.datetime.strptime(request.POST.get('fecha_desde'), '%d/%m/%Y').strftime("%d de %B de %Y")
        else:
            desde = "el origen de los tiempos"
        fecha_hasta = request.POST.get('fecha_hasta')
    else:
        presentacion = '1' # En un GET la presentación por defecto será Individual
        g= True
        fecha_desde = (datetime.date.today() - timedelta(datetime.date.today().day - 1)).strftime("%Y-%m-%d")
        desde = (datetime.date.today() - timedelta(datetime.date.today().day - 1)).strftime("%d de %B de %Y")
    try:
        hasta = datetime.datetime.strptime(fecha_hasta, '%d/%m/%Y').strftime("%d de %B de %Y")
    except:
        hasta = datetime.date.today().strftime("%d de %B de %Y")

    # Filtrando donadores
    donadores_list = []
    donadores = Donador.objects.filter(activo=1)
    for donador in donadores:
        donaciones = Donacion.objects.filter(donador=donador)
        if g:
            donaciones = donaciones.filter(fecha_ingreso__gte=fecha_desde)
        if request.POST.get('fecha_desde'):
            a = request.POST.get('fecha_desde')
            donaciones = donaciones.filter(fecha_ingreso__gte=a[6:] + "-" + a[3:5] + "-" + a[:2])
        if request.POST.get('fecha_hasta'):
            b = request.POST.get('fecha_hasta')
            donaciones = donaciones.filter(fecha_ingreso__lte=b[6:] + "-" + b[3:5] + "-" + b[:2])
        donadores_list.append([donador, donaciones])
    context = {
        'desde': desde,
        'hasta': hasta,
        'donadores_list': donadores_list,
        'presentacion': presentacion
    }
    return render(request, 'reportes/donaciones.html', context)

class ReporteDonadoresPDFView(View):

    template_name = 'reportes/lista_donadores.html' # the template

    def get(self, request, *args, **kwargs):

        donadores = Donador.objects.exclude(activo = False).defer('nombre', 'apellido', 'telefono', 'email', 'institucion')

        context = {
            'donadores' : donadores,
        }

        response = PDFTemplateResponse(request=request, template=self.template_name, filename="reporte_donadores.pdf", context= context,
                                       show_content_in_browser=True, cmd_options = {
                'margin-top' : 10,
                'zoom' : 1,
                'viewport-size' : '1366 x 513',
                'javascript-delay': 1000,
                'footer-center' : '[page]/[topage]',
                'no-stop-slow-scripts' : True
            },
                                       )
        return response

@login_required
@permission_required('control.change_dosis')
def lista_donadores(request):
    donadores = Donador.objects.exclude(activo=False)
    context = {
        'donadores': donadores,
    }
    return render(request, 'reportes/reporte_donadores.html', context)

@login_required
@permission_required('control.change_dosis')
def reporte_inventario(request):
    medicamentos = Medicamento.objects.filter(estado='1').order_by('stock')
    fecha = datetime.date.today()
    lista_med = []
    for med in medicamentos:
        lotes = LoteMedicamento.objects.filter(medicamento=med, existencias__gt=0)
        # Calcular medicamentos vencidos
        cant = 0
        for lote in lotes:
            if lote.fecha_vencimiento < fecha:
                cant += lote.existencias
        lista_med.append([med, lotes, cant])

    context = {
        'lista_med': lista_med,
        'fecha': fecha.strftime("%d/%m/%Y")
    }

    return render(request, 'reportes/reporte_inventario.html', context)
