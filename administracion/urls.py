# -*- coding: utf8 -*-
"""
Nombre del Módulo: Archivo de urls para la app “administracion”
Autores: Francisco Rivas, Kevin Rodríguez, Fernando Rivas, Irving Oliva, Martin Valle
Última fecha de modificación: 04/10/2017
"""
from django.conf.urls import url
from django.views.generic import TemplateView
from . import views
from django.contrib.auth.decorators import permission_required

urlpatterns = [
    #url(r'^$', views.index, name='index'),
    # Inician urls para pacientes
    url(r'^pacientes/$', views.consultarPacientes, name='consultarPacientes'),
    url(r'^confirmar_desactivar_paciente/(?P<paciente_id>[0-9]+)$', views.confirmar_desactivar_paciente, name='confirmar_desactivar_paciente'),
    url(r'^desactivar_paciente/(?P<paciente_id>[0-9]+)$', views.desactivar_paciente, name='desactivar_paciente'),
    url(r'^actualizar_paciente/(?P<pk>\d+)$', views.PacienteUpdate.as_view(),name='PacienteUpdate'),
    url(r'^agregar_paciente/', views.registroPaciente, name='registrar_paciente'),
    url(r'^actualizar_responsable/(?P<paciente_id>[0-9]+)$', views.actualizar_responsable, name='actualizar_responsable'),
    # Finalizan urls para pacientes

    # Inician urls para empleados
    url(r'^empleados/$', views.consultarEmpleados, name='consultarEmpleados'),
    url(r'^confirmar_desactivar_empleado/(?P<empleado_id>[0-9]+)$', views.confirmar_desactivar_empleado, name='confirmar_desactivar_empleado'),
    url(r'^desactivar_empleado/(?P<empleado_id>[0-9]+)$', views.desactivar_empleado, name='desactivar_empleado'),
    url(r'^actualizar_empleado/(?P<pk>\d+)$', views.EmpleadoUpdate.as_view(),name='EmpleadoUpdate'),
    url(r'^agregar_empleado/', views.registroEmpledo.as_view(), name='registrar_empleado'),
    # Finalizan urls para empleados

    # Inician urls para usuarios
    url(r'^agregar_usuario/', views.registroUsuario, name='registrar_usuario'),
    # Finalizan urls para usuarios

    # Inician urls para servicios
    url(r'^agregar_servicio/$', views.AgregarServicio.as_view(), name='agregar_servicio'),
    url(r'^servicios/$', views.consultarServicio, name='consultarServicio'),
    url(r'^actualizar_servicio/(?P<pk>\d+)$', views.ModificarServicio.as_view(),name='ServicioUpdate'),
    # Finalizan urls para servicios

    # Inician urls para tarifas
    url(r'^agregar_tarifa1/$', views.AgregarTarifa1.as_view(), name='agregar_tarifa1'),
    url(r'^agregar_tarifa2/$', views.AgregarTarifa2.as_view(), name='agregar_tarifa2'),
    url(r'^tarifas1/$', views.consultarTarifa1, name='consultarTarifa1'),
    url(r'^actualizar_tarifa1/(?P<pk>\d+)$', views.ModificarTarifa1.as_view(),name='Tarifa1Update'),
    url(r'^tarifas2/$', views.consultarTarifa2, name='consultarTarifa2'),
    url(r'^actualizar_tarifa2/(?P<pk>\d+)$', views.ModificarTarifa2.as_view(),name='Tarifa2Update'),
    # Finalizan urls para tarifas

    # Inician urls para Estudio Socio-Economico
    url(r'^agregar_estudio/(?P<id>[0-9]+)$', views.registroEstudio, name='registro_socio'),
    url(r'^actualizar_estudio/(?P<pk>\d+)$', views.ModificarEstudio.as_view(),name='EstudioUpdate'),
    # Finalizan urls para Estudio Socio-Economico

    # Inician urls para especialidades
    url(r'^agregar_especialidad/$', views.AgregarEspecialidad.as_view(), name='agregar_especialidad'),
    url(r'^especialidad/$', views.consultarEspecialidad, name='consultarEspecialidad'),
    url(r'^actualizar_especialidad/(?P<pk>\d+)$', views.ModificarEspecialidad.as_view(),name='EspecialidadUpdate'),
    url(r'^confirmar_desactivar_especialidad/(?P<especialidad_id>[0-9]+)$', views.confirmar_desactivar_especialidad, name='confirmar_desactivar_especialidad'),
    url(r'^desactivar_especialidad/(?P<especialidad_id>[0-9]+)$', views.desactivar_especialidad, name='desactivar_especialidad'),
    # Finalizan urls para especialidades

    # Inician Urls para Expediente Medico
    url(r'^expediente/(?P<pk>\d+)/$', views.expediente, name='expediente'),
    url(r'^expediente/pre_diagnostico/(?P<pk>\d+)/$', views.PreDiagnostico.as_view(), name='pre_diagnostico'),
    # Finalizan urls para Expediente Medico

    # Inician Urls para Grupos de Apoyo
    url(r'^grupos_apoyo/participantes/(?P<grupo_apoyo_id>\d+)/$', views.asistencia_participantes, name='asistencia_participantes'),
    url(r'^agregar_grupo/$', views.AgregarGrupo.as_view(), name='agregar_grupo'),
    url(r'^grupos_apoyo/(?P<pk>\d+)$', views.ModificarGrupo.as_view(),name='GrupoApoyoUpdate'),
    url(r'^grupos_apoyo/$', views.consultarGrupo, name='consultarGrupos'),
    url(r'^agregar_participante/$', views.AgregarParticipante.as_view(), name='agregar_participante'),
    url(r'^participantes/(?P<pk>\d+)$', views.ModificarParticipante.as_view(),name='ParticipanteUpdate'),
    url(r'^participantes/$', views.consultarParticipantes, name='consultarParticipantes'),
    url(r'^confirmar_desactivar_participante/(?P<participante_id>[0-9]+)$', views.confirmar_desactivar_participante, name='confirmar_desactivar_participante'),
    url(r'^desactivar_participante/(?P<participante_id>[0-9]+)$', views.desactivar_participante, name='desactivar_participante'),
    # Finalizan Urls para Grupos de Apoyo

    # Inicio de las URL'S para la generacion de los pdf
    url(r'^reportes/$', views.reportes, name='reportes'),
    url(r'^reportes/consultas_medicos$', views.consultas_medicos, name='consultas_medicos'),
    url(r'^reportes/consultas_especialidades$', views.consultas_especialidades, name='consultas_especialidades'),
    url(r'^reportes/examenes_realizados$', views.examenes_realizados, name='examenes_realizados'),
    url(r'^reportes/reporte_medicamentos$', views.reporte_inventario, name='reporte_medicamentos'),
    url(r'^reportes/donaciones/$', views.donaciones, name='donaciones'),
    url(r'^reportes/listado_donadores$', views.lista_donadores, name='listadoDonadores'),
    # url(r'^reportes/examenes$', views.ReporteExamenesPDFView.as_view(), name='reportesEx'),
    # url(r'^reportes/especialidades/$', views.ReporteEspecialidadesPDFView.as_view(), name='reporteEsp'),

    url(r'^agregar_ponente/$', views.AgregarPonente.as_view(), name='agregar_ponente'),
    url(r'^ponentes/$', views.consultarPonentes, name='consultarPonentes'),
    url(r'^ponentes/(?P<pk>\d+)$', views.ModificarPonente.as_view(),name='PonenteUpdate'),
    url(r'^confirmar_desactivar_ponente/(?P<ponente_id>[0-9]+)$', views.confirmar_desactivar_ponente, name='confirmar_desactivar_ponente'),
    url(r'^desactivar_ponente/(?P<ponente_id>[0-9]+)$', views.desactivar_ponente, name='desactivar_ponente'),
]
