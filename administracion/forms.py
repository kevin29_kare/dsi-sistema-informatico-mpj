# -*- coding: utf8 -*-
"""
Nombre del Módulo: Archivo de formularios para la app “administracion”
Autores: Francisco Rivas, Kevin Rodríguez, Irving Oliva
Última fecha de modificación: 04/10/2017
"""
from django import forms
from django.contrib.auth.models import User
from material import *
from .models import Paciente, Empleado, Responsable, Expediente, EstadoCivil, Socioeconomico, Tarifa, Servicio

class PacienteForm(forms.ModelForm):
    class Meta:
        model = Paciente
        fields = ['nombre', 'apellido', 'dui', 'nit', 'sexo', 'fecha_nacimiento', 'telefono', 'departamento', 'municipio', 'direccion', 'estado_civil','email']
        widgets = {
            'estado_civil' : forms.RadioSelect(),
            'sexo': forms.RadioSelect(),
        }

    layout = Layout(Fieldset('Información del Paciente:',
                             Row('nombre', 'apellido'), Row('dui', 'nit'), Row('fecha_nacimiento', 'telefono'),
                             Row('sexo', 'estado_civil'), Row('email','departamento'), Row('municipio','direccion')))

    def __init__(self, *args, **kwargs):
        super(PacienteForm, self).__init__(*args, **kwargs)
        self.fields['estado_civil'].choices = ((x.id, x.nombre) for x in EstadoCivil.objects.all())
        self.fields['sexo'].choices = (('M', 'Masculino'),('F', 'Femenino'))

class UsuarioForm(forms.Form):
    username = forms.CharField(error_messages={'required': 'Campo obligatorio'}, max_length=30, label='Username')
    email = forms.EmailField(label="E-mail")
    password1 = forms.CharField(error_messages={'required': 'Campo obligatorio'}, widget=forms.PasswordInput, label='Contraseña')
    password2 = forms.CharField(error_messages={'required': 'Campo obligatorio'}, widget=forms.PasswordInput, label='Contraseña (de nuevo)')
    layout = Layout(Fieldset('Registrar Usuario:',
                             Row('username', 'email'), Row('password1', 'password2')))

    def clean_username(self):
        existing = User.objects.filter(username__iexact=self.cleaned_data['username'])
        if existing.exists():
            raise forms.ValidationError("Ya existe username con este nombre")
        else:
            return self.cleaned_data['username']

    def clean_email(self):
        existing = User.objects.filter(email__iexact=self.cleaned_data['email'])
        if existing.exists():
            raise forms.ValidationError("Este email ya esta en uso, por favor, ingresa otro")
        return self.cleaned_data['email']


class Empleado(forms.ModelForm):
    class Meta:
        model = Responsable
        fields = ['nombre', 'apellido', 'parentesco']

class ExpedienteForm(forms.ModelForm):
    class Meta:
        model = Expediente
        fields = ['num_expediente', 'diagnostico', 'tratamiento', 'lugar']

    layout = Layout(Fieldset('Expediente:', Row('num_expediente'), 'diagnostico', 'tratamiento', 'lugar'))

    # def __init__(self, *args, **kwargs):
    #     super(ExpedienteForm, self).__init__(*args, **kwargs)
    #     self.fields['num_expediente'].widget.attrs['readonly'] = True

class ResponsableForm(forms.ModelForm):
    class Meta:
        model = Responsable
        fields = ['nombre', 'apellido', 'parentesco','telefono']

    layout = Layout(Fieldset('Información del Responsable:', Row('nombre', 'apellido'), Row('parentesco','telefono')))


class EstudioForm(forms.ModelForm):
    class Meta:
        model =Socioeconomico
        fields = ['vivienda', 'vehiculo', 'trabaja', 'esp_trabajo', 'estudia', 'negocio', 'pensionado', 'servicios', 'tarifa']
        widgets = {
            'servicios' : forms.CheckboxSelectMultiple(),
            'tarifa' : forms.Select(),
        }
    def __init__(self, *args, **kwargs):
        super(EstudioForm, self).__init__(*args, **kwargs)
        self.fields['tarifa'].choices = ((x.id, x.nombre) for x in Tarifa.objects.filter(tarifa_padre_id__isnull=True))
        self.fields['servicios'].choices = ((x.id, x.nombre.title) for x in Servicio.objects.all())
