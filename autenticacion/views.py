# -*- coding: utf8 -*-
"""
Nombre del Módulo: Archivo de vistas para la app “autenticacion”
Autores: Francisco Rivas, Kevin Rodríguez, Fernando Rivas
Última fecha de modificación: 04/10/2017
"""
from django.shortcuts import render, reverse, redirect
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User, Permission
from django.views.generic.edit import DeleteView, UpdateView
from django.contrib.auth.decorators import login_required, permission_required
from  django.contrib.auth.hashers import make_password
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.template import RequestContext
# Create your views here.

# Inician vistas para inicio de sesion
def mpj_login(request):
    mensaje = ""
    if request.POST:

        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/base')

        else:
            mensaje = "Usuario o Contraseña Incorrecto"
            return render(request, 'autenticacion/login.html', {'mensaje': mensaje, })
    else:
        return render(request, 'autenticacion/login.html', {'mensaje': mensaje, })


def mpj_logout(request):
    logout(request)
    return HttpResponseRedirect('/login')

@login_required
def base(request):
    return render(request, 'base.html',)
# Finalizan Vistas para inicio de sesion

# Inician vistas para gestion de usuarios
@login_required
def consultar_usuarios(request):
    if request.user.has_perm('auth.change_user') or request.user.has_perm('auth.delete_user') or \
            request.user.has_perm('auth.add_permission', 'auth.change_permission','auth.delete_permission')  :
        user_list=User.objects.all()
        return render(request, 'autenticacion/consultaUsuarios.html', {'user_list': user_list, })
    else:
        return HttpResponseRedirect('/login/?next=/usuarios/')

@login_required
#@permission_required(('auth.add_permissions','auth.delete_permissions','auth.change_permissions'))
@permission_required(('auth.add_permission','auth.change_permission','auth.delete_permission'))
def gestionar_permisos(request,user_id):
    user = User.objects.get(pk=user_id)
    if request.POST:
        if request.POST.getlist('pre-selected-options'):
            lista=request.POST.getlist('pre-selected-options')
            user.user_permissions.set(lista)
        else:
            user.user_permissions.clear()
    all_permissions=Permission.objects.all() #Sí son instancias
    user_permission_list=list(user.get_all_permissions()) #No son instancias
    has_not_permission_list = []
    has_permission_list = []
    for permission in all_permissions:
        b = False  # No tiene el permiso
        for user_permission in user_permission_list:
            if permission.codename in user_permission:
                b=True #Sí tiene el permiso

        if b: #¿Tiene el Permiso?
            has_permission_list.append(permission)
        else:
            has_not_permission_list.append(permission)

    return render(request, 'autenticacion/gestionarPermisos.html', {'has_permission_list': has_permission_list, 'has_not_permission_list': has_not_permission_list, })

class Eliminar_user(SuccessMessageMixin, PermissionRequiredMixin, DeleteView):
    model=User
    permission_required = 'auth.delete_user'

    def get_success_url(self):
        return reverse("consultar_usuarios")

class UserUpdate(SuccessMessageMixin, PermissionRequiredMixin, UpdateView):
    model = User
    fields = ['first_name', 'last_name', 'email',]
    template_name_suffix = '_update_form'
    success_message = "Usuario modificado con éxito"
    permission_required = 'auth.change_user'

    def get_success_url(self):
        return reverse("consultar_usuarios")

@login_required
def change_password(request):
    mensaje=""
    user = request.user
    if request.POST:
        if request.POST.get('contraseña'):
            if request.POST.get('contraseña') == request.POST.get('contraseña2'):
                user.password = make_password(request.POST.get('contraseña'), None, 'argon2')
            else:
                mensaje = "Las contraseñas no coinciden, vuelva a intentarlo"
                return render(request, "autenticacion/cuenta.html", {'nombre': user.first_name, 'apellido': user.last_name, 'email': user.email, 'mensaje': mensaje})

        if request.POST.get('nombres'):
            user.first_name = request.POST.get('nombres')
        if request.POST.get('apellidos'):
            user.last_name = request.POST.get('apellidos')
        if request.POST.get('correo'):
            user.email = request.POST.get('correo')
        try:
            user.save()
            mensaje = "Datos modificados con éxito"
        except:
            mensaje = "Error al modificar datos"


    user=User.objects.get(pk=user.id)
    return render(request, "autenticacion/cuenta.html", {'nombre': user.first_name, 'apellido': user.last_name, 'email': user.email, 'mensaje': mensaje})
# Finalizan vistas para gestion de usuarios
