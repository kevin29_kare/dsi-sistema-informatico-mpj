# -*- coding: utf8 -*-
"""
Nombre del Módulo: Archivo de urls para la app “autenticacion”
Autores: Francisco Rivas, Kevin Rodríguez, Fernando Rivas
Última fecha de modificación: 04/10/2017
"""
from django.conf.urls import url
from . import views
from django.contrib.auth.views import (
    password_reset, password_reset_done, password_reset_confirm, password_reset_complete
)

urlpatterns = [
    # Inician urls para inicio de sesion
    url(r'^login/$', views.mpj_login, name='mpj_login'),
    url(r'^logout/$', views.mpj_logout, name='mpj_logout'),
    url(r'^base/$', views.base, name='base'),
    # Finalizan urls para inicio de sesion

    # Inician urls para gestion de usuarios
    url(r'^usuarios/$', views.consultar_usuarios, name='consultar_usuarios'),
    url(r'^actualizar_usuario/(?P<pk>\d+)$', views.UserUpdate.as_view(), name='UserUpdate'),
    url(r'^cuenta/$', views.change_password, name='change_password'),
    url(r'^user_confirm_delete/(?P<pk>\d+)$', views.Eliminar_user.as_view(),name='EliminarUser'),
    url(r'^permisos/(?P<user_id>[0-9]+)$', views.gestionar_permisos, name='permisos' ),
    url(r'^reset-password/$', password_reset, {'post_reset_redirect': 'password_reset_done'}, name='reset_password'),
    url(r'^reset-password/done/$', password_reset_done, name='password_reset_done'),
    url(r'^reset-password/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', password_reset_confirm, {'post_reset_redirect': 'password_reset_complete'}, name='password_reset_confirm'),
    url(r'^reset-password/complete/$', password_reset_complete, name='password_reset_complete')
    # Finalizan urls para gestion de usuarios
]
